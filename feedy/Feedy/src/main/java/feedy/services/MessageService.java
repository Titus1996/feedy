package feedy.services;

import java.util.Set;

import feedy.domain.Message;
import feedy.domain.User;

public interface MessageService extends CRUDService<Message>{

	Set<Message> findByReceiver(User receiver);

}
