package feedy.services;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.Message;
import feedy.domain.User;
import feedy.repositories.MessageRepository;

@Service
public class MessageServiceImpl implements MessageService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	MessageRepository messageRepository;
	
	@Override
	public List<Message> listAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Message getById(Integer id) {
		logger.info("getById called : {]", id);

		Message result = messageRepository.getOne(id);

		logger.info("result:{}", result);
		return result;
	}

	@Override
	public Message saveOrUpdate(Message domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return messageRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		messageRepository.delete(id);
	}
	
	@Override
	public Set<Message> findByReceiver(User belonger) {

		logger.info("findByTo called:{}", belonger);

		Set<Message> message = messageRepository.findByReceiver(belonger);

		logger.info("findByTo result:{}", message);
		return message;

	}

}
