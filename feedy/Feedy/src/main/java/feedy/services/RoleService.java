package feedy.services;

import feedy.domain.Role;

public interface RoleService extends CRUDService<Role> {
	Role findByName(String name);
}
