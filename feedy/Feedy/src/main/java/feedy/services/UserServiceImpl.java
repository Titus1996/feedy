package feedy.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.User;
import feedy.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserRepository userRepository;

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<User> listAll() {
		logger.info("listAll called");

		List<User> users = userRepository.findAll();

		logger.info("listAll result:{}", users);
		return users;
	}

	@Override
	public User getById(Integer id) {
		logger.info("getById called {}", id);

		User user = userRepository.findOne(id);

		logger.info("getById:{}", user);
		return user;
	}

	@Override
	public User saveOrUpdate(User domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return userRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		userRepository.delete(id);
	}

	public User findByName(String name) {
		logger.info("findByName called:{}", name);
		User user = userRepository.findByUserName(name).get(0);

		logger.info("findByName result:{}", name);
		return user;
	}

}
