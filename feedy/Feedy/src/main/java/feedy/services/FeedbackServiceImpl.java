package feedy.services;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.Feedback;
import feedy.domain.User;
import feedy.repositories.FeedbackRepository;

@Service
public class FeedbackServiceImpl implements FeedbackService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	FeedbackRepository feedbackRepository;

	@Override
	public List<Feedback> listAll() {
		logger.info("listAll called");

		List<Feedback> feedbacks = feedbackRepository.findAll();

		logger.info("listAll result:{}", feedbacks);
		return feedbacks;
	}

	@Override
	public Feedback getById(Integer id) {
		logger.info("getById called : {]", id);

		Feedback result = feedbackRepository.getOne(id);

		logger.info("result:{}", result);
		return result;
	}

	@Override
	public Feedback saveOrUpdate(Feedback domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return feedbackRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		feedbackRepository.delete(id);

	}

	@Override
	public Set<Feedback> findByTo(User to) {

		logger.info("findByTo called:{}", to);

		Set<Feedback> feedback = feedbackRepository.findByTo(to);

		logger.info("findByTo result:{}", feedback);
		return feedback;

	}

	@Override
	public Set<Feedback> findByFrom(User from) {
		logger.info("findByFrom called:{}", from);

		Set<Feedback> feedback = feedbackRepository.findByFrom(from);

		logger.info("findByFrom result:{}", feedback);
		return feedback;
	}

	@Override
	public String getTemplateNameForFeedback(Feedback feedback) {
		logger.info("getTemplateNameForFeedback called:{}",feedback);

		String templateName = feedback
				.getAnswers()
				.stream()
				.findFirst()
				.get()
				.getQuestion()
				.getTemplate()
				.getName();

		logger.info("getTemplateNameForFeedback result:{}", templateName);
		return templateName;
	}

}
