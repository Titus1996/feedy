package feedy.services;

import java.util.Set;

import feedy.domain.Feedback;
import feedy.domain.User;

public interface FeedbackService extends CRUDService<Feedback>{

	Set<Feedback> findByTo(User to);
	
	Set<Feedback> findByFrom(User from);

	String getTemplateNameForFeedback(Feedback feedback);
}
