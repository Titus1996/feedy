package feedy.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.Template;
import feedy.repositories.TemplateRepository;

@Service
public class TemplateServiceImpl implements TemplateService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private TemplateRepository templateRepository;

	@Autowired
	public void setTemplateRepository(TemplateRepository templateRepository) {
		this.templateRepository = templateRepository;
	}

	@Override
	public Template getById(Integer id) {
		logger.info("getById called {}", id);

		Template template = templateRepository.findOne(id);

		logger.info("getById:{}", template);
		return template;
	}

	@Override
	public Template saveOrUpdate(Template domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return templateRepository.save(domainObject);
	}

	@Override
	public Template findByName(String name) {

		logger.info("findByName called:{}", name);
		List<Template> list = templateRepository.findByName(name);
		if (list.isEmpty())
			return null;
		Template template = list.get(0);
		logger.info("findByName result:{}", template);
		return template;
	}

	@Override
	public List<Template> listAll() {
		logger.info("listAll called");

		List<Template> templates = templateRepository.findAll();
		logger.info("listAll result:{}", templates);
		return templates;
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		templateRepository.delete(id);
	}

}
