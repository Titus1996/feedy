package feedy.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.Answer;
import feedy.repositories.AnswerRepository;

@Service
public class AnswerServiceImpl implements AnswerService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	AnswerRepository answerRepository;

	@Override
	public List<Answer> listAll() {
		logger.info("ListAll called");
		List<Answer> answers = answerRepository.findAll();
		logger.info("result:{}", answers);
		return answers;

	}

	@Override
	public Answer getById(Integer id) {
		logger.info("getById called {}", id);

		Answer answer = answerRepository.findOne(id);

		logger.info("getById", answer);
		return answer;
	}

	@Override
	public Answer saveOrUpdate(Answer domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return answerRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		answerRepository.delete(id);

	}

}
