package feedy.services;

import feedy.domain.Template;

public interface TemplateService extends CRUDService<Template> {
	Template findByName(String name);
	Template getById(Integer id);
}
