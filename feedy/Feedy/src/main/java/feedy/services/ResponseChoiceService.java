package feedy.services;

import feedy.domain.ResponseChoice;

public interface ResponseChoiceService extends CRUDService<ResponseChoice> {
}
