package feedy.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.Team;
import feedy.repositories.TeamRepository;

@Service
public class TeamServiceImpl implements TeamService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private TeamRepository teamRepository;

	@Autowired
	public void setTeamRepository(TeamRepository teamRepository) {
		this.teamRepository = teamRepository;
	}

	@Override
	public List<Team> listAll() {
		logger.info("listAll called");

		List<Team> teams = teamRepository.findAll();

		logger.info("listAll result:{]", teams);
		return teams;
	}

	@Override
	public Team getById(Integer id) {
		logger.info("getById called {}", id);

		Team team = teamRepository.findOne(id);

		logger.info("getById result:{}", team);
		return team;
	}

	@Override
	public Team saveOrUpdate(Team domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return teamRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		teamRepository.delete(id);
	}

	@Override
	public Team findByName(String teamName) {
		logger.info("findByName called:{}", teamName);

		Team team = teamRepository.findByName(teamName).get(0);

		logger.info("findByName result:{}", team);
		return team;
	}

}
