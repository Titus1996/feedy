package feedy.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import feedy.security.filters.FeedyCORSFilter;
import feedy.security.filters.JWTAuthenticationFilter;
import feedy.security.filters.JWTLoginFilter;
import feedy.security.other.FeedyAuthenticationProvider;
import feedy.security.other.FeedyLogoutSuccess;
import feedy.security.service.FeedyUserDetailsService;
import feedy.security.service.TokenAuthenticationService;
import waffle.servlet.spi.BasicSecurityFilterProvider;
import waffle.servlet.spi.NegotiateSecurityFilterProvider;
import waffle.servlet.spi.SecurityFilterProvider;
import waffle.servlet.spi.SecurityFilterProviderCollection;
import waffle.spring.NegotiateSecurityFilter;
import waffle.spring.NegotiateSecurityFilterEntryPoint;
import waffle.spring.WindowsAuthenticationProvider;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;

@Configuration
@ComponentScan(basePackages = { "feedy.controllers" })
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String CONSOLE_URL = "/console/**";

	@Autowired
	private FeedyLogoutSuccess logoutSuccess;

	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;

	@Autowired
	private FeedyUserDetailsService feedyUsersDetailService;

	@Autowired
	private FeedyAuthenticationProvider feedyAuthenticationProvider;

	@Bean
	public WindowsAuthProviderImpl waffleWindowsAuthProvider() {
		return new WindowsAuthProviderImpl();
	}

	@Bean
	public NegotiateSecurityFilterProvider negotiateSecurityFilterProvider(
			WindowsAuthProviderImpl windowsAuthProvider) {
		return new NegotiateSecurityFilterProvider(windowsAuthProvider);
	}

	@Bean
	public BasicSecurityFilterProvider basicSecurityFilterProvider(WindowsAuthProviderImpl windowsAuthProvider) {
		return new BasicSecurityFilterProvider(windowsAuthProvider);
	}

	@Bean
	public SecurityFilterProviderCollection waffleSecurityFilterProviderCollection(
			NegotiateSecurityFilterProvider negotiateSecurityFilterProvider,
			BasicSecurityFilterProvider basicSecurityFilterProvider) {
		SecurityFilterProvider[] securityFilterProviders = { negotiateSecurityFilterProvider,
				basicSecurityFilterProvider };
		return new SecurityFilterProviderCollection(securityFilterProviders);
	}

	@Bean
	public NegotiateSecurityFilterEntryPoint negotiateSecurityFilterEntryPoint(
			SecurityFilterProviderCollection securityFilterProviderCollection) {
		NegotiateSecurityFilterEntryPoint negotiateSecurityFilterEntryPoint = new NegotiateSecurityFilterEntryPoint();
		negotiateSecurityFilterEntryPoint.setProvider(securityFilterProviderCollection);
		return negotiateSecurityFilterEntryPoint;
	}

	@Bean
	public NegotiateSecurityFilter waffleNegotiateSecurityFilter(
			SecurityFilterProviderCollection securityFilterProviderCollection) {
		NegotiateSecurityFilter negotiateSecurityFilter = new NegotiateSecurityFilter();
		negotiateSecurityFilter.setProvider(securityFilterProviderCollection);
		return negotiateSecurityFilter;
	}

	// This is required for Spring Boot so it does not register the same filter
	// twice
	@Bean
	public FilterRegistrationBean waffleNegotiateSecurityFilterRegistration(
			NegotiateSecurityFilter waffleNegotiateSecurityFilter) {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(waffleNegotiateSecurityFilter);
		registrationBean.setEnabled(false);
		return registrationBean;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers(CONSOLE_URL).permitAll().anyRequest().authenticated().and().formLogin()
				.loginPage("/login").permitAll().usernameParameter("username").passwordParameter("password").and()
				.logout().logoutUrl("/logout").invalidateHttpSession(true).logoutSuccessHandler(logoutSuccess)
				.permitAll().and()
				.addFilterBefore(new FeedyCORSFilter(), ChannelProcessingFilter.class)
				.addFilterBefore(new JWTAuthenticationFilter(tokenAuthenticationService),
						UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(
						new JWTLoginFilter("/login", authenticationManager(), tokenAuthenticationService,
								feedyUsersDetailService, feedyAuthenticationProvider),
						UsernamePasswordAuthenticationFilter.class);

		http.exceptionHandling();
		http.csrf().disable();
		http.headers().frameOptions().disable();

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		WindowsAuthenticationProvider windowsAuthenticationProvider = new WindowsAuthenticationProvider();
		windowsAuthenticationProvider.setAuthProvider(this.waffleWindowsAuthProvider());
		auth.authenticationProvider(windowsAuthenticationProvider);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(CONSOLE_URL);
	}

}
