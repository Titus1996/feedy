package feedy.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.Message;
import feedy.domain.User;
 @Repository
public interface MessageRepository extends JpaRepository<Message, Integer>{

	Set<Message> findByReceiver(User receiver);

}
