package feedy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.ResponseChoice;

@Repository
public interface ResponseChoiceRepository extends JpaRepository<ResponseChoice, Integer>{
}
