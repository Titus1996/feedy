package feedy.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.Template;

@Repository
public interface TemplateRepository extends JpaRepository<Template, Integer> {
	public List<Template> findByName(String name);
}
