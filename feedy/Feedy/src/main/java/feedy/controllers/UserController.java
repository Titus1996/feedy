package feedy.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.converters.UserConverter;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.dtos.TeamDto;
import feedy.dtos.UserDto;
import feedy.services.TeamService;
import feedy.services.UserService;

@RestController
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;
	@Autowired
	private TeamService teamService;
	@Autowired
	private UserConverter userConverter;

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/usersByTeam", method = RequestMethod.GET)
	public List<TeamDto> getUsersByTeam() {
		logger.info("UsersByTeam called");

		List<TeamDto> teamList = teamService.listAll()
				.stream()
				.filter(team->!team.getUsers().isEmpty())
				.map(team -> new TeamDto(team.getName(), new ArrayList<>()))
				.collect(Collectors.toList());

		List<UserDto> userList = userService.listAll()
				.stream()
				.map(u1 -> userConverter.convertToDto(u1))
				.collect(Collectors.toList());

		for (UserDto userDto : userList)
			for (TeamDto teamDto : teamList) {
				if (teamDto.getTeamName().equals(userDto.getTeam()))
					teamDto.addUserToList(userDto);
			}

		logger.info("result:{}", teamList);
		return teamList;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/deleteUser/{userId}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable int userId) {
		logger.info("deleteUser called: userid:{}", userId);

		userService.delete(userId);
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public void saveUser(@RequestBody UserDto userDto) {
		logger.info("addUser called:{}", userDto);

		User user = userConverter.convertFromDto(userDto);

		logger.info("addUser result:{}", userService.saveOrUpdate(user));

	}

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/getAllUsers",method = RequestMethod.GET)
	public Set<UserDto> getAllUsers(){
		logger.info("getAllUsers called");

		Set<UserDto> users = userService
				.listAll()
				.stream()
				.map(user->userConverter.convertToDto(user))
				.collect(Collectors.toSet());

		logger.info("getAllUsers res:{}",users);
		return users;
	}


    @PreAuthorize("hasRole('SUL')")
    @RequestMapping(value = "/getTeamUsers",method = RequestMethod.GET)
    public Set<UserDto> getTeamUsers(Principal principal){
        logger.info("getTeamUsers called");
        logger.info("{}",userService.findByName(principal.getName()).getTeam().getSul());
        teamService.findByName("R_ERO_SU_Java").getSul().add(userService.findByName("boti"));
        Set<UserDto> users = teamService
                .listAll()
                .stream()
                .filter(team->
                        team.getSul()
                                .stream()
                                .map(User::getUsername)
                                .anyMatch(username->username.equals(principal.getName())))
                .flatMap(team->team.getUsers().stream())
                .map(user->userConverter.convertToDto(user))
                .collect(Collectors.toSet());

        logger.info("getTeamUsers res:{}",users);
        return users;
    }

}
