package feedy.controllers;

import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.converters.MessageConverter;
import feedy.domain.Message;
import feedy.domain.User;
import feedy.dtos.MessageDto;
import feedy.services.MessageService;
import feedy.services.UserService;

@RestController
public class MessageController {

	@Autowired
	private MessageService messageService;
	@Autowired
	private UserService userService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MessageConverter messageConverter;
	
	@PreAuthorize("hasAnyRole('NORMAL', 'SUL')")
	@RequestMapping(value = "getMessages/{receiver}", method = RequestMethod.GET)
	public Set<MessageDto> getMessages(@PathVariable String receiver){
		
		User userreceiver = userService.findByName(receiver);
		Set<Message> messages = messageService.findByReceiver(userreceiver);
		return messages.stream().map(mDto -> messageConverter.convertToDto(mDto)).collect(Collectors.toSet());
	}
	
	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
	public void sendMessage(@RequestBody MessageDto messageDto) {
		logger.info("sendMessage called:{}", messageDto);
	
		Message message = messageConverter.convertFromDto(messageDto);
		
		logger.info("sendMessage result:{} {}" , messageService.saveOrUpdate(message), message.getId());
	}
	
	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/dismissMessage/{id}", method = RequestMethod.POST)
	public void dismissMessage(@PathVariable int id) {
		logger.info("dismissMessage called:{}", id);
	
		Message message = messageService.getById(id);
		message.setDismissed(true);
		
		logger.info("dismissMessage result:{}", messageService.saveOrUpdate(message));
	}
	

	
}
