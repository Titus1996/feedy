package feedy.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.domain.Role;
import feedy.services.RoleService;

@RestController
public class RoleController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RoleService roleService;

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public List<String> getRoles() {
		logger.info("getRoles called");
		List<String> roles = roleService.listAll().stream().map(r -> (Role) r).map(Role::getName)
				.collect(Collectors.toList());
		logger.info("getRoles result:{}", roles);
		return roles;
	}

}
