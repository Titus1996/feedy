package feedy.controllers;

import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.converters.QuestionConverter;
import feedy.converters.ResponseChoiceConverter;
import feedy.domain.Question;
import feedy.domain.ResponseChoice;
import feedy.dtos.QuestionDto;
import feedy.dtos.ResponseChoiceDto;
import feedy.services.QuestionService;

@RestController
public class QuestionController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private QuestionService questionService;

	@Autowired
	private QuestionConverter questionConverter;

	private int position;

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/saveQuestion", method = RequestMethod.POST)
	public void save(@RequestBody QuestionDto questionDto) {
		logger.info("addQuestion called:{}", questionDto);

		Question question = questionConverter.convertFromDto(questionDto);

		logger.info("addTemplate convert:{}", questionService.saveOrUpdate(question));
	}

	@PreAuthorize("hasRole('NORMAL,SUL')")
	@RequestMapping(value = "/responseChoiceByQuestion/{questionId}", method = RequestMethod.GET)
	public Set<ResponseChoiceDto> getResponseChoiceByQuestion(@PathVariable int questionId) {
		ResponseChoiceConverter responseChoiceConverter = new ResponseChoiceConverter();

		Set<ResponseChoiceDto> responseChoicesDto = new TreeSet<>();

		Question question = questionService.getById(questionId);
		if (question != null) {
			Set<ResponseChoice> responseChoices = question.getResponseChoices();
			for (ResponseChoice responseChoice : responseChoices) {
				ResponseChoiceDto responseChoiceDto = responseChoiceConverter.convertToDto(responseChoice);
				responseChoicesDto.add(responseChoiceDto);
			}
		}
		return responseChoicesDto;

	}
}
