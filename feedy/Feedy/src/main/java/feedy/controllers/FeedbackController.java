package feedy.controllers;

import java.io.ByteArrayOutputStream;
import java.security.Principal;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.itextpdf.text.DocumentException;
import feedy.dtos.EmailDto;
import feedy.exception.ActionNotAllowedException;
import feedy.exception.InvalidFeedbackException;
import feedy.exception.InvalidIdException;
import feedy.mail.EmailSender;
import feedy.mail.PDFConverter;
import feedy.services.TemplateService;
import feedy.validators.FeedbackValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import feedy.converters.FeedbackConverter;
import feedy.domain.Feedback;
import feedy.domain.User;
import feedy.dtos.FeedbackDto;
import feedy.services.AnswerService;
import feedy.services.FeedbackService;
import feedy.services.UserService;
@RestController
public class FeedbackController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String ERNI_DOMAIN =  "@erni.ch";

	@Autowired
	private FeedbackValidator feedbackValidator;

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private UserService userService;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private AnswerService answerService;

	@Autowired
	private FeedbackConverter feedbackConverter;

	@Autowired
	private EmailSender emailSender;


	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/getSentFeedbacks", method = RequestMethod.GET)
	public Set<FeedbackDto> getSentFeedbacks(Principal principal) {
		logger.info("getSentFeedbacks called:{}",principal.getName());

		User user = userService.findByName(principal.getName());
		Set<Feedback> feedbacks = feedbackService.findByFrom(user);
		Set<FeedbackDto> feedbacksDto = feedbacks.stream()
                .filter(feedback -> !feedback.isGeneric())
                .sorted(Comparator.comparing(Feedback::getDate))
                .map(fDto -> feedbackConverter.convertToDto(fDto))
                .collect(Collectors.toSet());

		logger.info("getSentFeedbacks result:{}",feedbacksDto);
		return feedbacksDto;
	}

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/getReceivedUnreadFeedbacks",method = RequestMethod.GET)
	public Set<FeedbackDto> getReceivedUnreadFeedbacks(Principal principal){
		logger.info("getReceivedUnreadFeedbacks called:{}",principal.getName());
		User user = userService.findByName(principal.getName());

		Set<Feedback> feedbacks = feedbackService
                .findByTo(user)
                .stream()
                .filter(feedback -> !feedback.isRead())
                .collect(Collectors.toSet());
        Set<FeedbackDto> feedbacksDto = feedbacks
                .stream()
                .sorted(Comparator.comparing(Feedback::getDate))
                .map(fDto-> feedbackConverter.convertToDto(fDto))
                .collect(Collectors.toSet());


		logger.info("getReceivedUnreadFeedbacks res:{]",feedbacksDto);
		return feedbacksDto;
	}

	@PreAuthorize("hasAnyRole('SUL','NORMAL')")
	@RequestMapping(value = "/deleteFeedback/{id}", method = RequestMethod.DELETE)
	public boolean deleteFeedback(@PathVariable Integer id, Principal principal){
		logger.info("deleteFeedback:{} by {}",id,principal.getName());
		Feedback feedback = feedbackService.getById(id);
		if(feedback.isGeneric() && !userService.findByName(principal.getName()).getRole().getName().equals("SUL")){
		    return false;
        }
        if(!feedback.isGeneric() && !feedback.getTo().getUsername().equals(principal.getName())) {
            return false;
        }

		feedbackService.delete(id);
		logger.info("feedback deleted");
		return true;
	}

    @PreAuthorize("hasAnyRole('SUL')")
    @RequestMapping(value = "/getFeedbacksForUser/{id}", method = RequestMethod.GET)
    public Set<FeedbackDto> getFeedbacksForUser(@PathVariable Integer id, Principal principal) {
        logger.info("getFeedbacksForUser:{} by {}",id, principal.getName());

        User user;
        Set<FeedbackDto> feedbacksDto;
        user = userService.getById(id);
        Boolean isSulofUser=user.getTeam()
                .getSul()
                .stream()
                .map(User::getUsername)
                .anyMatch(username -> username.equals(principal.getName()));
        if(!isSulofUser){
            throw new ActionNotAllowedException("You are not a SUL of this user");
        }
        Set<Feedback> feedbacks = feedbackService.findByTo(user);
        feedbacksDto = feedbacks
                .stream()
                .sorted(Comparator.comparing(Feedback::getDate))
                .filter(feedback -> !feedback.isAnonymous())
                .map(fDto -> feedbackConverter.convertToDto(fDto))
                .collect(Collectors.toSet());

        logger.info("getFeedbacksForUser result:{}", feedbacksDto);
        return feedbacksDto;
    }

    @PreAuthorize("hasRole('SUL')")
    @RequestMapping(value = "/getGenericFeedbacks", method = RequestMethod.GET)
    public Set<FeedbackDto> getGenericFeedbacks(Principal principal){
	    logger.info("getGenericFeedbacks called:{}", principal.getName());

	    Set<FeedbackDto> feedbackDtos;
	    feedbackDtos = feedbackService
                .listAll()
                .stream()
                .filter(feedback -> feedback.getTo() == null)
                .filter(feedback -> feedback
                        .getAnswers()
                        .stream()
                        .anyMatch(answer -> answer.getQuestion().getTemplate().getIsGeneric()))
                .map(feedback -> {
                    FeedbackDto feedbackDto = feedbackConverter.convertToDto(feedback);
                    feedbackDto.setTemplateName(feedbackService.getTemplateNameForFeedback(feedback));
                    return feedbackDto; })
                .sorted(Comparator.comparing(FeedbackDto::getDate))
                .collect(Collectors.toSet());

	    logger.info("getGenericFeedbacks result:{}",feedbackDtos);
	    return feedbackDtos;
    }


	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/getReceivedFeedbacks", method = RequestMethod.GET)
	public Set<FeedbackDto> getReceivedFeedbacks(Principal principal) {
		logger.info("getRecivedFeedbacks:{}", principal.getName());
		User user;
		Set<FeedbackDto> feedbacksDto;
		try {
            user = userService.findByName(principal.getName());
        }
        catch(NullPointerException e){
		    throw new InvalidIdException("Invalid ID");
        }
		Set<Feedback> feedbacks = feedbackService.findByTo(user);
		feedbacksDto = feedbacks
                .stream()
                .sorted(Comparator.comparing(Feedback::getDate))
                .map(fDto -> feedbackConverter.convertToDto(fDto))
                .collect(Collectors.toSet());

		logger.info("getRecivedFeedbacks result:{}", feedbacksDto);
		return feedbacksDto;
	}

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/saveFeedback", method = RequestMethod.POST)
	public boolean saveFeedback(@RequestBody FeedbackDto feedbackDto,Principal principal) {
		logger.info("addFeedback called:{}{}", feedbackDto,principal.getName());
		try{
            feedbackValidator.validateFeedback(feedbackDto);
            Feedback feedback = feedbackConverter.convertFromDto(feedbackDto);

            if(principal.getName().equals(feedbackDto.getTo())){
                throw new ActionNotAllowedException("Cannot give yourself feedback");
            }
			if(!principal.getName().equals(feedbackDto.getFrom())) {
				throw new ActionNotAllowedException("Cannot give feedback from another person");
			}
			Boolean isGeneric = feedback
                    .getAnswers()
                    .stream()
                    .anyMatch(answer -> answer.getQuestion().getTemplate().getIsGeneric());
            if(isGeneric && !feedbackDto.getTo().equals("")){
            	throw new ActionNotAllowedException("Cannot give feedback to another person with a generic template");
			}

            Feedback feedbackRes = feedbackService.saveOrUpdate(feedback);
            if(feedbackRes == null)
                return false;
            feedbackRes.getAnswers().forEach(answer -> {
                answer.setFeedback(feedbackRes);
                logger.info("{}", answer.getFeedback());
                logger.info("{}", answerService.saveOrUpdate(answer));
            });
            logger.info("addFeedback result:{}",feedbackRes);
            return true;
        } catch (InvalidFeedbackException e) {
            logger.info("Invalid feedback" + e.toString());
            return false;
        }
	}

	@RequestMapping(value = "/markFeedback",method = RequestMethod.POST)
	public void markFeedbackAsRead(@RequestBody FeedbackDto feedbackDto,Principal principal){
		logger.info("markFeedback called:{}",feedbackDto);

		if(!principal.getName().equals(feedbackDto.getTo())){
			throw new ActionNotAllowedException("Invalid user");
		}
		Feedback feedback = feedbackService.getById(feedbackDto.getId());
		feedback.setRead(false);
		feedbackService.saveOrUpdate(feedback);

		logger.info("markfeedback end :{}",feedback);
	}

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public void sendEmail(@RequestBody EmailDto emailDto){
		logger.info("sendEmail called:{}",emailDto);

		String templateUsed = templateService.getById(emailDto.getTemplateUsed()).getName();
		final String targetFeedbackUser = emailDto.getTo() + ERNI_DOMAIN;
		Set<String> targetsToEmail = new HashSet<>();
		targetsToEmail.add(targetFeedbackUser);
		final String MESSAGE_TEXT = "Hello "+ targetFeedbackUser+"\n You received a feedback from "+emailDto.getFrom() +".The template used was:"+templateUsed;
        ByteArrayOutputStream feedbackStream = new ByteArrayOutputStream();

        try {
            PDFConverter.convertFeedbackToPDF(feedbackStream, emailDto.getFeedback());
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        emailSender.sendEmail("Feedback recived",MESSAGE_TEXT,targetsToEmail, feedbackStream);

		logger.info("sendEmail end");
	}

}
