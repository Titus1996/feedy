package feedy.controllers;

import java.util.*;
import java.util.stream.Collectors;

import feedy.exception.InvalidTemplateException;
import feedy.validators.TemplateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.converters.QuestionConverter;
import feedy.converters.TemplateConverter;
import feedy.domain.Question;
import feedy.domain.Template;
import feedy.dtos.QuestionDto;
import feedy.dtos.TemplateDto;
import feedy.exception.DuplicateTemplateNameException;
import feedy.services.QuestionService;
import feedy.services.TemplateService;

import javax.validation.constraints.Null;

@RestController
public class TemplateController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TemplateService templateService;

	@Autowired
	private TemplateConverter templateConverter;

	@Autowired
	private QuestionConverter questionConverter;

	@Autowired
	private QuestionService questionService;

	@Autowired
    private TemplateValidator  templateValidator;

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/templates", method = RequestMethod.GET)
	public List<TemplateDto> getTemplates() {
		logger.info("getTemplates called");
		List<TemplateDto> templates = templateService.listAll().stream().filter(t->!t.getIsDeprecated())
				.map(t -> templateConverter.convertToDto(t)).collect(Collectors.toList());
		logger.info("getTemplates result:{}", templates);
		return templates;
	}

    @PreAuthorize("hasAnyRole('NORMAL','SUL')")
    @RequestMapping(value = "/genericTemplates", method = RequestMethod.GET)
    public List<TemplateDto> getGenericTemplates() {
        logger.info("getGenericTemplates called");
        List<TemplateDto> templates  = new ArrayList<>();
        try {
			templates = templateService
					.listAll()
					.stream()
					.filter(t -> !t.getIsDeprecated())
					.filter(Template::getIsGeneric)
					.map(t -> templateConverter.convertToDto(t))
					.collect(Collectors.toList());
		}
		catch(NullPointerException ignored){
		}
        logger.info("getGenericTemplates result:{}", templates);
        return templates;
    }

	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/getNonGenericTemplates", method = RequestMethod.GET)
	public List<TemplateDto> getNonGenericTemplates() {
		logger.info("getNonGenericTemplates called");
		List<TemplateDto> templates = templateService
				.listAll()
				.stream()
				.filter(t->!t.getIsDeprecated() && !t.getIsGeneric())
				.map(t -> templateConverter.convertToDto(t))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		logger.info("getGenericTemplates result:{}", templates);
		return templates;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/saveTemplate", method = RequestMethod.POST)
	public TemplateDto save(@RequestBody TemplateDto templateDto) throws DuplicateTemplateNameException {
		logger.info("addTemplate called:{}", templateDto);

		try {
			templateValidator.validateTemplate(templateDto);
		} catch (InvalidTemplateException tempalte) {
			throw new RuntimeException("Invalid template");
		}
		Template template = templateConverter.convertFromDto(templateDto);
		template.setIsDeprecated(false);

		Optional<Template> templateOptional = Optional.ofNullable(templateService.findByName(template.getName()));
		if (templateOptional.isPresent()) {
			if(template.getId() == 0 || !template.getId().equals(templateOptional.get().getId()) && template.getName().equals(templateOptional.get().getName())){
				logger.info("addTemplate result: A template with the same name already exists !");
				throw new DuplicateTemplateNameException("Duplicate template name not allowed");
			}
		}

		template.getQuestions().forEach(question -> {
			question.setTemplate(template);
			if (question.getAnswers() == null) {
				return;
			}
			if (question.getAnswers().isEmpty() && !"FREETEXT".equals(question.getQuestionType().getName())) {
				logger.info("Invalid question(only freetext can have empty array for response choices):{}",
						question);
				template.getQuestions().remove(question);
			}
			if (!"FREETEXT".equals(question.getQuestionType().getName())) {
				question.getResponseChoices().forEach(response -> response.setQuestion(question));
			}
		});
		logger.info("addTemplate result:{}", templateService.saveOrUpdate(template));

		return templateDto;
	}
	
	@PreAuthorize("hasAnyRole('NORMAL','SUL')")
	@RequestMapping(value = "/getQuestionsByTemplate/{templateId}", method = RequestMethod.GET)
	public Set<QuestionDto> getQuestionsByTemplate(@PathVariable int templateId) {
		Set<Question> questions;
		logger.info("Template id:{}", templateId);
		Set<QuestionDto> questionsDto = new TreeSet<>(Comparator.comparing(QuestionDto::getId));
		Template template = templateService.getById(templateId);
		logger.info("Template:{}", template);
		if (template != null) {
			try {
				logger.info("Template:{}", template);
				questions = template.getQuestions();
				for (Question question : questions) {
					logger.info("Question:{}", question);
					QuestionDto questionDto = questionConverter.convertToDto(question);
					questionsDto.add(questionDto);
					logger.info("QuestionDto:{}", questionDto);
				}

			} catch (NullPointerException e) {
				logger.info("Null question list in template \n get Questions of template ID:{}", templateId);
			}
		}

		logger.info("getQuestionsByTemplate result:{}", questionsDto);
		return questionsDto;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/templateByName/{name}", method = RequestMethod.GET)
	public Template getTemplateByName(@PathVariable String name) {
		Template template = templateService.findByName(name);
		if (template == null)
			return new Template();
		else
			return template;

	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/deleteTemplate/{id}", method = RequestMethod.DELETE)
	public void deleteTemplate(@PathVariable int id) {
		logger.info("deleteTemplate called: {}", id);

		Template toDeprecate = templateService.getById(id);
		toDeprecate.setIsDeprecated(true);
		templateService.saveOrUpdate(toDeprecate);

		logger.info("deleteTemplate end");
	}

}
