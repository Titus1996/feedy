package feedy.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.domain.Template;
import feedy.exception.InvalidIdException;
import feedy.services.TemplateService;
import feedy.statistics.FeedbackStatistics;
import feedy.statistics.QuestionStatistics;
import feedy.statistics.TemplateStatistics;

@RestController
public class ReportsController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String RESULT = "result:{}";

	@Autowired
	private FeedbackStatistics feedbackStatistic;

	@Autowired
	private QuestionStatistics questionStatistics;

	@Autowired
	private TemplateStatistics templateStatistics;

	@Autowired
	private TemplateService templateService;

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/getTopAnswersForTemplateQuestions/{templateId}", method = RequestMethod.GET)
	public List<Map<String, Map<String, Long>>> getTopAnswersForTemplateQuestions(@PathVariable int templateId) {
		logger.info("getTopAnswersForTemplateQuestions called:{}", templateId);

		Optional<Template> templateOptional = Optional.ofNullable(templateService.getById(templateId));
		Template template = templateOptional.orElseThrow(() -> new InvalidIdException("Invalid Template Id"));
		List<Map<String, Map<String, Long>>> mostAnswers = template.getQuestions().stream()
				.filter(question -> questionStatistics.getTopAnswersForQuestion(question) != null).map(question -> {
					Map<String, Map<String, Long>> mappedQuestion = new HashMap<String, Map<String, Long>>();
					mappedQuestion.put(question.getText(), questionStatistics.getTopAnswersForQuestion(question));
					return mappedQuestion;
				}).collect(Collectors.toList());

		logger.info(RESULT, mostAnswers);
		return mostAnswers;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "getProcentageForFeedback/{templateId}", method = RequestMethod.GET)
	public long getProcentageForFeedback(@PathVariable int templateId) {
		logger.info("getProcentageForFeedback:{}", templateId);

		Optional<Template> templateOptional = Optional.ofNullable(templateService.getById(templateId));
		Template template = templateOptional.orElseThrow(() -> new InvalidIdException("Invalid Feedback Id"));
		Long procentage = feedbackStatistic.getPercentageForTemplate(template);

		logger.info(RESULT, procentage);
		return procentage;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "getTemplatesByUsage", method = RequestMethod.GET)
	public Map<String, Integer> getTemplatesByUsage() {
		logger.info("getTemplatesByUsage");

		Map<String, Integer> templatesByUsage = templateStatistics.getTemplatesByUsage();

		logger.info(RESULT, templatesByUsage);
		return templatesByUsage;
	}
}
