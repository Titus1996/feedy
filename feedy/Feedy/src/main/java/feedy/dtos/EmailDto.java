package feedy.dtos;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class EmailDto {
    private String From;
    private String To;
    private boolean Anonymous;
    private int TemplateUsed;
    private FeedbackDto feedback;
}
