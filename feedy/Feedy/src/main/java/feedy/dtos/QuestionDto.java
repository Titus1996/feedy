package feedy.dtos;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QuestionDto {
	private int id;
	private String questionText;
	private String questionType;
	private String remark;
	private Set<ResponseChoiceDto> responseChoices;
	public QuestionDto(int id, String questionText, String questionType, String remark, Set<ResponseChoiceDto> responseChoiceDtos){
	    this.id = id;
	    this.questionText = questionText;
	    this.questionType = questionType;
	    this.remark = remark;
	    this.responseChoices = responseChoiceDtos;
    }
    private int min;
    private int max;
	private boolean isRequired;
}
