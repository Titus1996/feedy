package feedy.dtos;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TemplateDto {
	int id;
	private String name;
	private String author;
	private Set<QuestionDto> questions;
	private Boolean isGeneric;

	public TemplateDto(String name, String author, Set<QuestionDto> questions) {
		this.name = name;
		this.author = author;
		this.questions = questions;
	}
}
