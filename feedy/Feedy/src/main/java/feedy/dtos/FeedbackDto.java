package feedy.dtos;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FeedbackDto {

	private int id;
	private String from;
	private String to;
	private boolean anonymous;
	private String date;
	private Set<AnswerDto> answers;
	private boolean isGeneric;
	private String templateName;

	public FeedbackDto(String from, String to, boolean anonymous, String date, Set<AnswerDto> answers, Boolean isGeneric) {
		this.from = from;
		this.to = to;
		this.anonymous = anonymous;
		this.date = date;
		this.answers = answers;
		this.isGeneric = isGeneric;
	}

}
