package feedy.exception;

public class InvalidResponseChoiceException extends Exception {

    public InvalidResponseChoiceException(String message) {
        super(message);
    }

    public InvalidResponseChoiceException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
