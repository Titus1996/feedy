package feedy.exception;

public class InvalidAnswerException extends Exception {

    public InvalidAnswerException(String message) {
        super(message);
    }

    public InvalidAnswerException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
