package feedy.exception;

public class InvalidFeedbackException extends Exception {

    public InvalidFeedbackException(String message) {
        super(message);
    }

    public InvalidFeedbackException(String message, Throwable throwable) {
        super(message, throwable);
    }

}