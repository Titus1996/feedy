package feedy.exception;

public class InvalidQuestionException extends Exception {

    public InvalidQuestionException(String message) {
        super(message);
    }

    public InvalidQuestionException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
