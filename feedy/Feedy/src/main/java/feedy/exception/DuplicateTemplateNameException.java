package feedy.exception;

@SuppressWarnings("serial")
public class DuplicateTemplateNameException extends Exception {

	public DuplicateTemplateNameException(String message) {
		super(message);
	}

	public DuplicateTemplateNameException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
