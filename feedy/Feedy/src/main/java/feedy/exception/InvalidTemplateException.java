package feedy.exception;

public class InvalidTemplateException extends Exception {

    public InvalidTemplateException(String message) {
        super(message);
    }

    public InvalidTemplateException(String message, Throwable throwable) {
        super(message, throwable);
    }

}