package feedy.bootstrap;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import feedy.domain.Message;
import feedy.domain.Question;
import feedy.domain.QuestionType;
import feedy.domain.ResponseChoice;
import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.Template;
import feedy.domain.User;
import feedy.repositories.MessageRepository;
import feedy.repositories.QuestionTypeRepository;
import feedy.repositories.RoleRepository;
import feedy.repositories.TeamRepository;
import feedy.repositories.TemplateRepository;
import feedy.repositories.UserRepository;

@Component
public class Loader{

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TemplateRepository templateRepository;

	@Autowired
	private QuestionTypeRepository questionTypeRepository;
	
	@Autowired
	private MessageRepository messageRepository;

	public void init(){

		////////////////// QuestionTypes

		QuestionType singleChoiceQuestionType = new QuestionType("SINGLE_CHOICE");
		QuestionType multipleChoiceQuestionType = new QuestionType("MULTIPLE_CHOICE");
		QuestionType freetextQuestionType = new QuestionType("FREETEXT");

		questionTypeRepository.save(singleChoiceQuestionType);
		questionTypeRepository.save(multipleChoiceQuestionType);
		questionTypeRepository.save(freetextQuestionType);

		///////////////////// TEAM,ROLE

		Role userRole = new Role("NORMAL");
		Role sulRole = new Role("SUL");

		roleRepository.save(userRole);
		roleRepository.save(sulRole);

		///////////// TEMPLATES

		Template templateExample = new Template("BasicQuestions", "Jhon Smith", null);
		Template templateSecondExample = new Template("QuestionsSUL", "Hannah Silver", null);
		templateRepository.save(templateExample);
		templateRepository.save(templateSecondExample);

		//////////// ResponseChoices

		///////////////// single1

		ResponseChoice responseChoiceSingleChoiceQuestion11 = new ResponseChoice("bad");
		ResponseChoice responseChoiceSingleChoiceQuestion12 = new ResponseChoice("ok");
		ResponseChoice responseChoiceSingleChoiceQuestion13 = new ResponseChoice("good");
		ResponseChoice responseChoiceSingleChoiceQuestion14 = new ResponseChoice("excellent");

		Set<ResponseChoice> respChoiceSetSingle1 = new HashSet<>();
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion11);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion12);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion13);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion14);

		///////////////////////// multiple1
		ResponseChoice responseChoiceMultipleChoiceQuestion11 = new ResponseChoice("sociable");
		ResponseChoice responseChoiceMultipleChoiceQuestion12 = new ResponseChoice("good listener");
		ResponseChoice responseChoiceMultipleChoiceQuestion13 = new ResponseChoice("optimistic");
		ResponseChoice responseChoiceMultipleChoiceQuestion14 = new ResponseChoice("always in time");

		Set<ResponseChoice> respChoiceSetMultiple1 = new HashSet<>();
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion11);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion12);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion13);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion14);

		/////////// single2

		ResponseChoice responseChoiceSingleChoiceQuestion21 = new ResponseChoice("never");
		ResponseChoice responseChoiceSingleChoiceQuestion22 = new ResponseChoice("sometimes");
		ResponseChoice responseChoiceSingleChoiceQuestion23 = new ResponseChoice("often");
		ResponseChoice responseChoiceSingleChoiceQuestion24 = new ResponseChoice("almost all the time");

		Set<ResponseChoice> respChoiceSetSingle2 = new HashSet<>();
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion21);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion22);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion23);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion24);

		////////////// multiple2

		ResponseChoice responseChoiceMultipleChoiceQuestion21 = new ResponseChoice("gym");
		ResponseChoice responseChoiceMultipleChoiceQuestion22 = new ResponseChoice("football");
		ResponseChoice responseChoiceMultipleChoiceQuestion23 = new ResponseChoice("photography");
		ResponseChoice responseChoiceMultipleChoiceQuestion24 = new ResponseChoice("movies");

		Set<ResponseChoice> respChoiceSetMultiple2 = new HashSet<>();
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion21);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion22);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion23);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion24);

		////////// QUESTIONS

		Question firstQuestion = new Question(templateExample, "Is he/she a sociable person?", freetextQuestionType,
				null, null);

		Question singleChoiceQuestion1 = new Question(templateExample, "How good is he/she at cooking?",
				singleChoiceQuestionType, respChoiceSetSingle1, null);
		Question multipleChoiceQuestion1 = new Question(templateExample, "What are their skills at work?",
				multipleChoiceQuestionType, respChoiceSetMultiple1, null);

		Question question1 = new Question(templateSecondExample, "Describe their attitude at work.",
				freetextQuestionType, null, null);
		Question singleChoiceQuestion2 = new Question(templateSecondExample, "How often is he/she  late for work?",
				singleChoiceQuestionType, respChoiceSetSingle2, null);
		Question multipleChoiceQuestion2 = new Question(templateSecondExample, "What are their hobbies?",
				multipleChoiceQuestionType, respChoiceSetMultiple2, null);

		Set<Question> questionSet = new HashSet<>();
		questionSet.add(firstQuestion);

		questionSet.add(singleChoiceQuestion1);
		questionSet.add(multipleChoiceQuestion1);

		Set<Question> questionSet1 = new HashSet<>();
		questionSet1.add(question1);
		questionSet1.add(singleChoiceQuestion2);
		questionSet1.add(multipleChoiceQuestion2);

		templateExample.setQuestions(questionSet);
		templateRepository.save(templateExample);

		templateSecondExample.setQuestions(questionSet1);
		templateRepository.save(templateSecondExample);

		//////////////////// USERS
		User dotNETUserSUL = new User("Debbie Frost", "222222", sulRole, teamRepository.findOne(0));
		dotNETUserSUL.setFullName("Debbie Frost");
		userRepository.save(dotNETUserSUL);
	
	}

}
