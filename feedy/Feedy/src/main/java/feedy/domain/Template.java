package feedy.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import org.eclipse.jdt.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Template")
@AllArgsConstructor
public class Template extends BaseEntity {

	private String name;
	private String author;
	private Boolean isGeneric = false;

	@OneToMany(cascade = CascadeType.ALL)
	private Set<Question> questions;

	private Boolean isDeprecated = false;

	public Template() {}

	public Template(String name, String author, @Nullable Set<Question> questions) {

		this.name = name;
		this.author = author;
		this.questions = questions;
	}

    public Template(int id, String name, String author, @Nullable Set<Question> questions,Boolean isGeneric){
	    this.id = id;
        this.name = name;
        this.author = author;
        this.questions = questions;
        this.isGeneric = isGeneric;
    }

	public Template(String name, String author, @Nullable Set<Question> questions,Boolean isGeneric) {

		this.name = name;
		this.author = author;
		this.questions = questions;
		this.isGeneric = isGeneric;
	}

	public String toString() {
		return "{Template:" + name + " " + author + " " + getId() + " isGeneric:" + isGeneric + "}";
	}

}
