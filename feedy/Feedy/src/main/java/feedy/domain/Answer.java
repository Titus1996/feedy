package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Answer")
@NoArgsConstructor
public class Answer extends BaseEntity {

	private String text;

	int score = -1;

	@ManyToOne(fetch = FetchType.LAZY)
	private Question question;

	@ManyToOne(fetch = FetchType.LAZY)
	private Feedback feedback;

	public Answer(String text, Question question, int score) {
		this.text = text;
		this.question = question;
		this.score = score;
	}

	public Answer(String text, Question question) {
		this.text = text;
		this.question = question;
	}

	public String toString() {
		return "{Answer:" + text + " " + getId() + "}";
	}

}
