package feedy.domain;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.eclipse.jdt.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "Team")
@NoArgsConstructor
@AllArgsConstructor
public class Team extends BaseEntity {

	private String name;

	@OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true,fetch = FetchType.EAGER)
	private Set<User> sul;

	@OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true,fetch = FetchType.EAGER)
	private Set<User> users;

	public Team(String name, @Nullable Set<User> sul) {
		this.name = name;
		this.sul = sul;
	}

	public String toString() {
		return "{Team:" + name + " " + getId() + "}";
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Team))
			return false;
		if (obj == this)
			return true;
		return (this.id == ((Team) obj).getId() && this.getName().equals(((Team) obj).getName()));
	}

	public int hashCode() {
		return id;
	}
}
