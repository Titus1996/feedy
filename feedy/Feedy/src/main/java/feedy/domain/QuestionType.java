package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "QuestionType")
public class QuestionType extends BaseEntity {

	private String name;
	public QuestionType() {
	}

	public QuestionType(String name) {
		this.name = name;
	}

	public String toString() {
		return "{QuestionType:" + name + " " + getId() + "}";
	}

}
