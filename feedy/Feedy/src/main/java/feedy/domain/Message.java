package feedy.domain;



import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@Table(name = "Message")
public class Message extends BaseEntity{
	@OneToOne
	private User receiver;
	@OneToOne
	private User sender;
	private String content;
	private Boolean dismissed = false;
	
	Message(){
		super();
	}

}
