package feedy.domain;

import java.util.Set;

import javax.persistence.*;

import lombok.NoArgsConstructor;
import org.eclipse.jdt.annotation.Nullable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Question")
public class Question extends BaseEntity {

	@ManyToOne
	private Template template;
	private String text;
	private boolean isRequired;
	//If RATING Question type
	private int min = 0;
	private int max = 0;


	@OneToOne
	private QuestionType questionType;

	//IF FREETEXT
	private String remark;

	//If SINGLE OR MULTIPLE CHOICE
	@OneToMany(cascade = CascadeType.ALL)
	private Set<ResponseChoice> responseChoices;
	@OneToMany(cascade = CascadeType.ALL)
	private Set<Answer> answers;

	public Question(Template template, String text, QuestionType questionType) {
		this.text = text;
		this.questionType = questionType;
		this.template = template;
	}

	public Question(String questionText, QuestionType questionType, String remark, Set<ResponseChoice> responseChoices) {
		this.questionType = questionType;
		this.text = questionText;
		this.remark = remark;
		this.responseChoices = responseChoices;
	}

	public Question(Template template, String text, QuestionType questionType,
			@Nullable Set<ResponseChoice> responseChoices, @Nullable Set<Answer> answers) {
		this.answers = answers;
		this.questionType = questionType;
		this.text = text;
		this.responseChoices = responseChoices;
		this.template = template;
	}

	public Question(String questionText, QuestionType questionType, String remark, Set<ResponseChoice> responseChoices, int min, int max) {
		this.questionType = questionType;
		this.text = questionText;
		this.remark = remark;
		this.responseChoices = responseChoices;
		this.min = min;
		this.max = max;
	}

    public Question(String questionText, QuestionType questionType, String remark, Set<ResponseChoice> responseChoices, int min, int max, boolean isRequired) {
        this.questionType = questionType;
        this.text = questionText;
        this.remark = remark;
        this.responseChoices = responseChoices;
        this.min = min;
        this.max = max;
        this.isRequired = isRequired;
    }

	public String toString() {
		return "{Question:" + getTemplate() + " " + text + " " + questionType + " " + getId() + "}";
	}

}
