package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.eclipse.jdt.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "`User`")
public class User extends BaseEntity {

	private String username;
	private String password;
	private String fullName;
	private String email;

	@OneToOne
	private Role role;

	@ManyToOne
	private Team team;

	public User() {
	}

	public User(String username, String password, Role role, Team team, @Nullable String email,
			@Nullable String fullName) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.role = role;
		this.team = team;
		this.fullName = fullName;
	}

	public User(String username, String password, Role role, @Nullable Team team) {

		this.username = username;
		this.password = password;
		this.role = role;
		this.team = team;
	}

	public User(int id, String username, String password, Role role, @Nullable Team team) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
		this.team = team;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", fullName=" + fullName + ", email=" + email + ", role=" + role
				+ ", team=" + team + "]";
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof User))
			return false;
		if (obj == this)
			return true;
		User user = (User) obj;
		return this.username.equals(user.getUsername()) && this.email.equals(((User) obj).email);
	}

	public int hashCode() {
		return id;
	}

}
