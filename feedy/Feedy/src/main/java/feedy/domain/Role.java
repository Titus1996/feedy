package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Role")
public class Role extends BaseEntity {

	private String name;

	public Role() {
	}

	public Role(String name) {
		this.name = name;
	}

	public String toString() {
		return "{Role:"+name + " " + getId()+"}";
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Role))
			return false;
		if (obj == this)
			return true;
		Role role = (Role) obj;
		return (this.name.equals(role.getName()));
	}
	
	public int hashCode() {
		return id;}
}
