package feedy.domain;
import java.sql.Date;
import java.util.Set;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "Feedback")
public class Feedback extends BaseEntity {

	private boolean anonymous;
	private Date date;
	private boolean isRead = false;
	private boolean isGeneric;
	@OneToOne
	private User from;
	@OneToOne
	private User to;
	@OneToMany(cascade = CascadeType.ALL)
	private Set<Answer> answers;

	public Feedback(User from, User to, boolean anonymous, Date date, Set<Answer> answers, Boolean isGeneric) {
		this.from = from;
		this.to = to;
		this.anonymous = anonymous;
		this.date = date;
		this.answers = answers;
		this.isGeneric = isGeneric;
	}

	public String toString() {
		return "{Feedback:" + from + " " + to + " " + isAnonymous() + " " + getDate() + " " + getId() + "}";
	}

}
