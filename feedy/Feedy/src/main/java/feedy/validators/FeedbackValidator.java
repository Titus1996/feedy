package feedy.validators;
import feedy.dtos.FeedbackDto;
import feedy.exception.InvalidAnswerException;
import feedy.exception.InvalidFeedbackException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
public class FeedbackValidator {

    @Autowired
    private AnswerValidator answerValidator;

    public void validateFeedback(FeedbackDto feedbackDto) throws InvalidFeedbackException {
        Date current = new Date();
        Date date = new Date();

        try {
            LengthValidator.validateString(feedbackDto.getFrom());
            if(!feedbackDto.isGeneric())
                LengthValidator.validateString(feedbackDto.getTo());
            date = new Date(Long.parseLong(feedbackDto.getDate().replace(".","").replace(",","")));
            feedbackDto
                    .getAnswers()
                    .forEach(answerDto -> {
                        if(answerDto.getQuestion().isRequired()) {
                            try {
                                answerValidator.validateAnswer(answerDto);
                            } catch (InvalidAnswerException e) {
                                throw new RuntimeException(e.toString());
                            }
                        }
                    });
        }
        catch (Exception e){
            throw new InvalidFeedbackException(e.toString());
        }
        if(current.compareTo(date) == 0){
            throw new InvalidFeedbackException("Invalid date");
        }

    }

}
