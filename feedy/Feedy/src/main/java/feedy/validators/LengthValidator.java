package feedy.validators;


public class LengthValidator {
    private static final int MAX_LENGTH = 255;
    private static final int ZERO = 0;

    public static void validateString(String text) throws RuntimeException {
        int length = text.length();
        if(length > MAX_LENGTH || length == ZERO){
            throw new RuntimeException("Invalid text length");
        }
    }
}
