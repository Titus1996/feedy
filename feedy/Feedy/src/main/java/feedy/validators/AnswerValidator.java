package feedy.validators;

import feedy.dtos.AnswerDto;
import feedy.exception.InvalidAnswerException;
import feedy.exception.InvalidQuestionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnswerValidator {

    @Autowired
    private QuestionValidator questionValidator;

    public void validateAnswer(AnswerDto answerDto) throws InvalidAnswerException{
        try{
            LengthValidator.validateString(answerDto.getText());
            questionValidator.validateQuestion(answerDto.getQuestion());
        }

        catch(InvalidQuestionException e){
            throw new InvalidAnswerException(e.toString());
        }
        catch (Exception e){
            throw new InvalidAnswerException(e.toString() + " at answer " + answerDto.getId());
        }
    }
}
