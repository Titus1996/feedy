package feedy.validators;

import feedy.dtos.TemplateDto;
import feedy.exception.InvalidQuestionException;
import feedy.exception.InvalidTemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TemplateValidator {

    @Autowired
    private QuestionValidator questionValidator;

    public void validateTemplate(TemplateDto templateDto) throws InvalidTemplateException {
        try {
            LengthValidator.validateString(templateDto.getName());
            LengthValidator.validateString(templateDto.getAuthor());
            templateDto.getQuestions()
                    .stream()
                    .forEach(questionDto -> {
                        try {
                            questionValidator.validateQuestion(questionDto);
                        } catch (InvalidQuestionException e) {
                            throw new RuntimeException(e.toString());
                        }
                    });
        }
        catch(Exception e){
            throw new InvalidTemplateException(e.toString());
        }
    }
}
