package feedy.validators;
import feedy.dtos.ResponseChoiceDto;
import feedy.exception.InvalidResponseChoiceException;
import org.springframework.stereotype.Component;

@Component
public class ResponseChoiceValidator {

    public void validateResponseChoice(ResponseChoiceDto responseChoiceDto) throws InvalidResponseChoiceException {
        try {
            LengthValidator.validateString(responseChoiceDto.getResponseChoiceText());
        }
        catch(Exception e){
            throw new InvalidResponseChoiceException(e.toString() + ":"+responseChoiceDto.getResponseChoiceText());
        }
    }
}
