package feedy.validators;

import feedy.dtos.QuestionDto;
import feedy.exception.InvalidQuestionException;
import feedy.exception.InvalidResponseChoiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuestionValidator {

    @Autowired
    private ResponseChoiceValidator responseChoiceValidator;

    public void validateQuestion(QuestionDto questionDto) throws InvalidQuestionException{
        try {
            LengthValidator.validateString(questionDto.getQuestionText());
            try {
                questionDto.getResponseChoices().stream()
                        .forEach(responseChoiceDto -> {
                            try {
                                responseChoiceValidator.validateResponseChoice(responseChoiceDto);
                            } catch (InvalidResponseChoiceException e) {
                                throw new RuntimeException(e.toString());
                            }
                        });
            }
            catch (Exception e){
                throw new InvalidQuestionException(e.toString() + ":" + questionDto.getId());
            }
        }
        catch(Exception e){
            throw new InvalidQuestionException(e.toString() + " " + questionDto.getQuestionText());
        }
    }
}
