package feedy.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.dtos.UserDto;
import feedy.services.RoleService;
import feedy.services.TeamService;
import feedy.services.UserService;

@Component
public class UserConverter {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private TeamService teamService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;

	private final String EMPTY_STRING = "";

	public UserDto convertToDto(User user) {
		logger.info("convertToDto called {}", user);
		UserDto userDto = new UserDto(user.getId(), user.getUsername(), user.getTeam().getName(),
				user.getRole().getName(), user.getFullName(), user.getEmail());
		logger.info("convertToDto result {}", userDto);
		return userDto;
	}

	public User convertFromDto(UserDto userDto) {
		logger.info("convertFromDto called:{}", userDto);
		Team userTeam = teamService.findByName(userDto.getTeam());
		Role userRole = roleService.findByName(userDto.getRole());
		User user = null;

		if (userDto.getId() != 0) {
			user = new User(userDto.getId(), userDto.getName(), EMPTY_STRING, userRole, userTeam);
		} else {
			user = new User(userDto.getName(), EMPTY_STRING, userRole, userTeam);
		}

		logger.info("convertFromDto result:{}", user);
		return user;
	}

}
