package feedy.converters;

import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Question;
import feedy.domain.Template;
import feedy.dtos.QuestionDto;
import feedy.dtos.TemplateDto;

@Component
public class TemplateConverter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private QuestionConverter questionConverter;

	public TemplateDto convertToDto(Template template) {
		Set<QuestionDto> questionsDto = template.getQuestions().stream().map(q -> questionConverter.convertToDto(q))
				.collect(Collectors.toSet());
		return new TemplateDto(template.getId(), template.getName(), template.getAuthor(), questionsDto, template.getIsGeneric());
	}

	public Template convertFromDto(TemplateDto templateDto) {
		logger.info("convertFromDto called:{}", templateDto);

		Set<Question> questions = templateDto.getQuestions().stream().map(q -> questionConverter.convertFromDto(q))
				.collect(Collectors.toSet());

		if(templateDto.getIsGeneric() == null){
			templateDto.setIsGeneric(false);
		}
		Template template;
		if(templateDto.getId() != 0)
			template = new Template(templateDto.getId(), templateDto.getName(), templateDto.getAuthor(), questions, templateDto.getIsGeneric());
		else
			template = new Template(templateDto.getName(), templateDto.getAuthor(), questions, templateDto.getIsGeneric());
		if (templateDto.getId() != 0) {
			template.setId(templateDto.getId());
		}

		logger.info("convertFromDto result:{}", template);
		return template;
	}
}
