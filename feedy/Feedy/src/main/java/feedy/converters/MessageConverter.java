package feedy.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Message;
import feedy.dtos.MessageDto;
import feedy.services.UserService;

@Component
public class MessageConverter {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@Autowired
	private UserService userService;
	
	public MessageDto convertToDto(Message message) {
		logger.info("convertToDto called:{}", message);
		MessageDto messageDto = new MessageDto(message.getId(), message.getReceiver().getUsername(), message.getSender().getUsername(), message.getContent(),  message.getDismissed());
		logger.info("convertToDto result:{}", messageDto);
		return messageDto;
	}
	
	public Message convertFromDto(MessageDto messageDto) {
		logger.info("convertFromDto called:{}", messageDto);
		
		Message message = new Message(userService.findByName(messageDto.getReceiver()),userService.findByName(messageDto.getSender()), messageDto.getContent(),  false);
		logger.info("convertFromDto result{}", message);
		return message;
	}
	
}
