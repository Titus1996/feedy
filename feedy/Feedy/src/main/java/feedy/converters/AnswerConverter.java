package feedy.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Answer;
import feedy.dtos.AnswerDto;
import feedy.services.QuestionService;

@Component
public class AnswerConverter {

	@Autowired
	private QuestionConverter questionConverter;

	@Autowired
	private QuestionService questionService;

	public AnswerDto convertToDto(Answer answer) {
		return new AnswerDto(answer.getId(), answer.getText(), questionConverter.convertToDto(answer.getQuestion()), answer.getScore());
	}

	public Answer convertFromDto(AnswerDto answerDto) {
		return new Answer(answerDto.getText(), questionService.getById(answerDto.getQuestion().getId()), answerDto.getScore());
	}
}
