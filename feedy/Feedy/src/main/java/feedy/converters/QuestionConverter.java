package feedy.converters;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Question;
import feedy.domain.QuestionType;
import feedy.domain.ResponseChoice;
import feedy.dtos.QuestionDto;
import feedy.dtos.ResponseChoiceDto;
import feedy.services.QuestionTypeService;

@Component
public class QuestionConverter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ResponseChoiceConverter responseChoiceConverter;

	@Autowired
	private QuestionTypeService questionTypeService;
		private String SINGLE_CHOICE_TYPE = "SINGLE_CHOICE";
		private String MULTIPLE_CHOICE_TYPE = "MULTIPLE_CHOICE";
		private String RATING_TYPE = "RATING";

	public QuestionDto convertToDto(Question question) {
		logger.info("convertToDto:{}", question);
		Set<ResponseChoiceDto> responseChoicesDto = new TreeSet<>(Comparator.comparing(ResponseChoiceDto::getId));
		int min=0;
		int max=0;
		int score =0;
		if(question.getQuestionType().getName().equals(SINGLE_CHOICE_TYPE) || question.getQuestionType().getName().equals(MULTIPLE_CHOICE_TYPE)){
			Set<ResponseChoice> responseChoices = question.getResponseChoices();
			for (ResponseChoice responseChoice : responseChoices) {
				ResponseChoiceDto responseChoiceDto = responseChoiceConverter.convertToDto(responseChoice);
				responseChoicesDto.add(responseChoiceDto);
			}
		}
        if(question.getQuestionType().getName().equals(RATING_TYPE)){
		    min = question.getMin();
		    max = question.getMax();
        }
		QuestionDto questionDto = new QuestionDto(question.getId(), question.getText(),
				question.getQuestionType().getName(),  question.getRemark(), responseChoicesDto,min,max, question.isRequired());
		logger.info("convertToDto:{}", questionDto);
		return questionDto;
	}

	public Question convertFromDto(QuestionDto questionDto) {
		logger.info("convertFromDto called:{}", questionDto);

		Set<ResponseChoice> responseChoices = questionDto.getResponseChoices().stream()
				.map(r -> responseChoiceConverter.convertFromDto(r)).collect(Collectors.toSet());

		QuestionType questionType = questionTypeService.findByName(questionDto.getQuestionType());
		int min = questionDto.getMin();
		int max = questionDto.getMax();

		Question question = new Question(questionDto.getQuestionText(), questionType,
                questionDto.getRemark(), responseChoices, min, max, questionDto.isRequired());
		if (questionDto.getId() != 0) {
			question.setId(questionDto.getId());
		}

		logger.info("convertFromDto result:{}", question);
		return question;
	}
}
