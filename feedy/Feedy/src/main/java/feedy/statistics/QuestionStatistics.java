package feedy.statistics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Answer;
import feedy.domain.Question;
import feedy.services.AnswerService;

@Component
public class QuestionStatistics {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String freeTextQuestionType = "FREETEXT";
	private static final String ratingQuestionType = "RATING";

	@Autowired
	private AnswerService answerService;

	public Map<String, Long> getTopAnswersForQuestion(Question question) {
		logger.info("getTopAnswers for question:{}", question);

		if (freeTextQuestionType.equals(question.getQuestionType().getName())) {
			return null;
		}
		Map<String, Long> mostUsedAnswers = new HashMap<>();
		if(ratingQuestionType.equals(question.getQuestionType().getName())){
			List<Answer> answersToQuestion = answerService.listAll().stream()
					.filter(answer -> answer.getQuestion().getId().equals(question.getId())).collect(Collectors.toList());
			double mean = answersToQuestion
					.stream()
					.mapToDouble(Answer::getScore)
					.average()
					.orElse(Double.NaN);
			long longMean = (long)mean;
			mostUsedAnswers.put("Average", longMean);
			return  mostUsedAnswers;

		}
		List<Answer> answersToQuestion = answerService.listAll().stream()
				.filter(answer -> answer.getQuestion().getId().equals(question.getId())).collect(Collectors.toList());

		answersToQuestion.stream().collect(Collectors.groupingBy(Answer::getText, Collectors.counting())).entrySet()
				.stream().sorted(Map.Entry.<String, Long>comparingByValue().reversed()).limit(3)
				.forEach(entry -> mostUsedAnswers.put(entry.getKey(), entry.getValue()));

		logger.info("result:{}", mostUsedAnswers);
		return mostUsedAnswers;
	}
}
