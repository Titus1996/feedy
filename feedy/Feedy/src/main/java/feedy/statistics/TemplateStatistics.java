package feedy.statistics;

import java.util.HashMap;
import java.util.Map;

import feedy.domain.Feedback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.services.FeedbackService;
import feedy.services.QuestionService;
import feedy.services.TemplateService;

@Component
public class TemplateStatistics {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private TemplateService templateService;

	public Map<String, Integer> getTemplatesByUsage() {
		logger.info("getTemplatesByUsage called");

		Map<String, Integer> templatesByUsage = new HashMap<>();
		templateService.listAll().forEach(template -> templatesByUsage.put(template.getName(), 0));
		questionService.listAll();
		feedbackService.listAll().stream().map(Feedback::getAnswers).forEach(answerSet -> {
			if (answerSet.isEmpty()) {
				return;
			}
			String name = answerSet.iterator().next().getQuestion().getTemplate().getName();
			templatesByUsage.put(name, templatesByUsage.get(name) + 1);
		});

		logger.info("getTemplateByUsage result:{}", templatesByUsage);
		return templatesByUsage;
	}

}
