package feedy.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Answer;
import feedy.domain.Template;
import feedy.services.AnswerService;
import feedy.services.UserService;

@Component
public class FeedbackStatistics {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private AnswerService answerService;
	
	public long getPercentageForTemplate(Template template) {
		logger.info("getPercentageForTemplate called:{}", template);

		long totalUsers = userService.listAll().size();
		List<Integer> feedbackIds = new ArrayList<>();
		template.getQuestions().forEach(question -> {
			List<Answer> answersToQuestion = answerService.listAll().stream().filter(a -> a.getQuestion().getId().equals(question.getId())).collect(Collectors.toList());
			answersToQuestion.forEach(answer -> {
			logger.info("{}",answer.getFeedback());
			if (!feedbackIds.contains(answer.getFeedback().getFrom().getId())) {
				feedbackIds.add(answer.getFeedback().getFrom().getId());
			}
		
		}
		);});
		logger.info("feedbackIds.size:{}", feedbackIds.size());
		logger.info("feedbackIds", feedbackIds);
		logger.info("totalUsers:{}", totalUsers);
		long result = (100 * feedbackIds.size()) / totalUsers;

		logger.info("result:{}", result);
		return result;
	}


}
