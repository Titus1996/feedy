package feedy.security.filters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;

import feedy.active_directory.ActiveDirectoryHelper;
import feedy.active_directory.ActiveDirectoryUserInfo;
import feedy.security.other.AccountCredentials;
import feedy.security.other.FeedyAuthenticationProvider;
import feedy.security.other.UserDetailsAdapter;
import feedy.security.service.FeedyUserDetailsService;
import feedy.security.service.TokenAuthenticationService;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

	private TokenAuthenticationService tokenAuthenticationService;

	private final String ROLE_STRING = "ROLE_";

	private final String SUL_ROLE = "_SUL";

	private FeedyUserDetailsService feedyUsersDetailService;

	private FeedyAuthenticationProvider feedyAuthenticationProvider;

	private ActiveDirectoryHelper activeDirectoryHelper;

	private final static String REQUESTEDFIELDS = "distinguishedName,userPrincipalName,telephoneNumber,mail,memberOf";
	
	private static final Logger LOGGER = Logger.getLogger(JWTLoginFilter.class.getName());

	public JWTLoginFilter(String url, AuthenticationManager authManager,
			TokenAuthenticationService tokenAuthenticationService, FeedyUserDetailsService feedyUsersDetailService,
			FeedyAuthenticationProvider feedyAuthenticationProvider) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
		this.tokenAuthenticationService = tokenAuthenticationService;
		this.feedyAuthenticationProvider = feedyAuthenticationProvider;
		this.feedyUsersDetailService = feedyUsersDetailService;
		activeDirectoryHelper = new ActiveDirectoryHelper();	
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {

		String result = new BufferedReader(new InputStreamReader(req.getInputStream())).lines()
				.collect(Collectors.joining("\n"));
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(Feature.AUTO_CLOSE_SOURCE, true);
		if (result.isEmpty()) {
			logger.info("Empty Credentials");
			return getAuthenticationManager()
					.authenticate(new UsernamePasswordAuthenticationToken("", "", Collections.emptyList()));
		}
		AccountCredentials creds = objectMapper.readValue(result, AccountCredentials.class);
		LOGGER.info("Credentials: " + creds.getUsername());
		Authentication toAuthenticate = new UsernamePasswordAuthenticationToken(creds.getUsername(),
				creds.getPassword(), Collections.emptyList());

		Authentication authenticated;
		try {
			authenticated = getAuthenticationManager().authenticate(toAuthenticate);
		} catch (AuthenticationServiceException exception) {
			LOGGER.info("attemptAuthentication exception " + exception);
			authenticated = feedyAuthenticationProvider.authenticate(toAuthenticate);
		}
		return authenticated;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) {
		LOGGER.info("success: "+  auth.getName() + " authenticated");
		String role = null;
		UserDetailsAdapter userDetails = null;
		
		try {
			userDetails=(UserDetailsAdapter) feedyUsersDetailService.loadUserByUsername(auth.getName().replace("ERNI\\", ""));
			List<GrantedAuthority> roles = new ArrayList(userDetails.getAuthorities());
			tokenAuthenticationService.addAuthentication(res,userDetails.getUsername(),roles.get(0).toString(),userDetails.getUser().getFullName());
		} catch (UsernameNotFoundException exception) {
			LOGGER.info("unsaved User: " + auth.getName() + " Exception: " + exception);
			ActiveDirectoryUserInfo userInfo = ActiveDirectoryUserInfo.getInstance(auth.getName(), REQUESTEDFIELDS);
			Map<String, String> infoMap = userInfo.getInfoMap();
			String mail = infoMap.get("mail");
			List<String> teams = activeDirectoryHelper.getTeams(infoMap.get("memberOf"));
			String name = activeDirectoryHelper.getName(infoMap.get("distinguishedName"));
			role = activeDirectoryHelper.getRoleFromTeam(teams);
			String team = teams.get(0);
			team = team.replace(SUL_ROLE,"");
			feedyUsersDetailService.saveUser(auth.getName(), role, team, mail, name);
			role = ROLE_STRING + role;
			tokenAuthenticationService.addAuthentication(res, auth.getName(), role, name);
		}

	}
}