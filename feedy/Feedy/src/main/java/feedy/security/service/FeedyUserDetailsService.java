package feedy.security.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.exception.RoleNotFoundException;
import feedy.repositories.RoleRepository;
import feedy.repositories.TeamRepository;
import feedy.repositories.UserRepository;
import feedy.security.other.UserDetailsAdapter;

@Service
public class FeedyUserDetailsService implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String UNIVERSAL_PASSWORD = "pass";
	private final String SUL_ROLE = "SUL";
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("loadUserByUsername called: {}", username);
		List<User> user = userRepository.findByUserName(username);
		if (user.isEmpty()) {
			throw new UsernameNotFoundException("No such user");
		}
		UserDetailsAdapter userDetails = new UserDetailsAdapter(user.get(0));

		return userDetails;
	}

	public void saveUser(String username, String roleName, String teamName, String email, String fullName) {
		logger.info("saveUser: {},role:{}, teamName:{},email:{},fullName:{}", username, roleName, teamName, email,
				fullName);

		username = username.replace("ERNI\\", "");
		Team team;
		Role role = Optional.ofNullable(roleRepository.findByName(roleName.replace("ROLE_", "")).get(0))
				.orElseThrow(() -> new RoleNotFoundException("Invalid Role Name"));
		if (teamRepository.findByName(teamName).isEmpty()) {
			team = new Team(teamName, new HashSet<>(),new HashSet<>());
			teamRepository.save(team);

		} else {
			team = teamRepository.findByName(teamName).get(0);
		}
		User newUser = new User(username, UNIVERSAL_PASSWORD, role, team, email, fullName);
        userRepository.save(newUser);

		if (role.getName().equals(SUL_ROLE)) {
			team.getSul().add(newUser);
			logger.info("User saved as SUL");
		}
		team.getUsers().add(newUser);
		teamRepository.save(team);
		logger.info("savedUser:{}", newUser);
	}

}
