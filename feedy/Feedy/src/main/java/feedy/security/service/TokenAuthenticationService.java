package feedy.security.service;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenAuthenticationService {
	static final long EXPIRATIONTIME = 864_000_000; // 10 days
	static final String SECRET = "ThisIsASecret";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FeedyUserDetailsService feedyUserDetailsService;

	public void addAuthentication(HttpServletResponse res, String username, String role, String fullName) {

		logger.info("addAuthentication:{},{}", username, role);

		String jwt = Jwts.builder().setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
				.signWith(SignatureAlgorithm.HS512, SECRET).compact();
		JSONObject json = new JSONObject();
		try {
			json.put("Role", role);
			json.put("Token", jwt);
			json.put("FullName",fullName);
		} catch (JSONException e) {
			logger.info("TokenAuthenticationService addAuthentication", e);
			e.printStackTrace();
		}
		logger.info("JSON: {} ", json);
		try {
			res.getWriter().append(json.toString());
		} catch (IOException e) {
			logger.info("TokenAuthenticationService addAuthentication", e);
		}
		logger.trace("Token:{}", jwt);
	}

	public Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			// parse the token.
			String userName = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
					.getBody().getSubject();
			userName = userName.replace("ERNI\\", "");
			logger.info("request from : {}", userName);
			UserDetails user = feedyUserDetailsService.loadUserByUsername(userName);
			return user != null
					? new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(),
							user.getAuthorities())
					: null;
		}
		return null;
	}
}
