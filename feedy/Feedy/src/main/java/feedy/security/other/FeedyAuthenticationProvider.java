package feedy.security.other;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import feedy.security.service.FeedyUserDetailsService;

@Component
public class FeedyAuthenticationProvider implements AuthenticationProvider {

	private FeedyUserDetailsService feedyUserDetailsService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	public void setFeedyUserDetailsService(FeedyUserDetailsService feedyUserDetailsService) {
		this.feedyUserDetailsService = feedyUserDetailsService;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();
		logger.info("{}", authentication);
		UserDetailsAdapter user = (UserDetailsAdapter) feedyUserDetailsService.loadUserByUsername(username);
		logger.info("{}", user);
		if (user.getPassword().equals(password)) {
			return new UsernamePasswordAuthenticationToken(username, password, user.getAuthorities());
		} else {
			throw new BadCredentialsException("Invalid password");
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
