package feedy.active_directory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActiveDirectoryHelper {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String PATTERN = "(?<=CN=).*?(?=,)";

	private final String SUL_ROLE = "SUL";

	private final String HR = "HR";

	private final String NORMAL_ROLE = "NORMAL";

	public List<String> getTeams(String teamsString) {
		logger.info("Searching for userInfo:{}", teamsString);

		List<String> teams = Arrays.asList(teamsString.split("\\|", -1));
		Pattern pattern = Pattern.compile(PATTERN);
		List<String> userTeams = new ArrayList<>();
		Matcher matcher;
		for (int i = 0; i < teams.size(); i++) {
			if (Arrays.asList(teams.get(i).split(",")).contains("OU=OrgChart")) {
				matcher = pattern.matcher(teams.get(i));
				matcher.find();
				userTeams.add(matcher.group());
			}
		}

		logger.info("userInfo result:{}", userTeams);
		return userTeams;
	}

	public String getRoleFromTeam(List<String> teamsList) {
		logger.info("getRoleFromTeam:{}", teamsList);
		boolean isSul = teamsList.stream().anyMatch(team->team.contains(SUL_ROLE) || team.contains(HR));

		if (isSul) {
			logger.info("getRoleFromTeam result:{}", SUL_ROLE);
			return SUL_ROLE;
		} else {
			logger.info("getRoleFromTeam result:{}", NORMAL_ROLE);
			return NORMAL_ROLE;
		}
	}

	public String getName(String nameString) {
		logger.info("getName called:{}", nameString);

		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(nameString);
		matcher.find();
		logger.info("matcher:{}", matcher);
		String name = matcher.group();

		logger.info("getName result:{}", name);
		return name;
	}
}
