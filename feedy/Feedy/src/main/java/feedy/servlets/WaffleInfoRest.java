package feedy.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import waffle.servlet.WaffleInfoServlet;

@RestController
public class WaffleInfoRest extends WaffleInfoServlet implements HttpRequestHandler {

	private static final long serialVersionUID = 1L;

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		super.getWaffleInfoResponse(request, response);
	}

}
