package feedy.mail;


import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;

import com.itextpdf.text.Document;
import org.springframework.stereotype.Component;

@Component
public class EmailSender {

	private static final Logger LOGGER = Logger.getLogger(EmailSender.class.getName());

	private final String smtpAuthUserName = "erni.notifier@erni.ro";
	private final String smtpAuthPassword = "1E26vewqD+!" ;
	private final String emailFrom = "erni.notifier@erni.ro";
	private final String HOST_NAME = "smtp-mail.outlook.com";
	private final String PORT = "587";
	private final String AUTH = "true";
	private final String START_TLS = "true";

	public void sendEmail(String pSubject, String pMessage, Set<String> pTo,ByteArrayOutputStream feedback) {
		Authenticator authenticator = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(smtpAuthUserName, smtpAuthPassword);
			}
		};
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.host", HOST_NAME);
		properties.setProperty("mail.smtp.port", PORT);
		properties.setProperty("mail.smtp.auth", AUTH);
		properties.setProperty("mail.smtp.starttls.enable", START_TLS);
		Session session = Session.getInstance(properties, authenticator);
		 session.setDebug(true); // enable debugging if needed

		try {
			Message message = new MimeMessage(session);
			MimeBodyPart textBodyPart = new MimeBodyPart();
			textBodyPart.setText(pMessage);
			//PDF file
			MimeBodyPart pdfBodyPart = new MimeBodyPart();
			byte[] bytes = feedback.toByteArray();
			DataSource dataSource = new ByteArrayDataSource(bytes,"application/pdf");
			pdfBodyPart.setDataHandler(new DataHandler(dataSource));
			pdfBodyPart.setFileName("feedback.pdf");

			//construct the mime multi part
			MimeMultipart mimeMultipart = new MimeMultipart();
			mimeMultipart.addBodyPart(textBodyPart);
			mimeMultipart.addBodyPart(pdfBodyPart);

			message.setFrom(new InternetAddress(emailFrom));
			InternetAddress[] to = convertAddresses(pTo);
			message.setRecipients(Message.RecipientType.TO, to);
			message.setSubject(pSubject);
			message.setContent(mimeMultipart);

			Transport.send(message);
		} catch (Exception exception) {
			LOGGER.info("Exception while sending email: " + exception.getMessage());
		}
	}

	private static InternetAddress[] convertAddresses(Set<String> pTo) {
		return pTo.stream().map(a -> {
			try {
				return new InternetAddress(a);
			} catch (AddressException e) {
				e.printStackTrace();
			}
			return null;
		}).toArray(InternetAddress[]::new);
	}

}