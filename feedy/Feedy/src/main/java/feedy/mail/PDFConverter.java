package feedy.mail;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import feedy.domain.Question;
import feedy.dtos.AnswerDto;
import feedy.dtos.FeedbackDto;
import feedy.dtos.QuestionDto;

import java.io.OutputStream;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class PDFConverter {
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 20,
            Font.BOLD);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);

    public static void convertFeedbackToPDF(OutputStream outputStream, FeedbackDto feedbackDto) throws DocumentException{
        Document document = new Document();
        PdfWriter.getInstance(document,outputStream);
        document.open();
        Paragraph fromParagraph = new Paragraph("From:"+feedbackDto.getFrom());
        fromParagraph.setFont(catFont);
        document.add(fromParagraph);
        Paragraph toParagraph = new Paragraph("To:"+feedbackDto.getTo());
        toParagraph.setFont(catFont);
        document.add(toParagraph);
        Set<QuestionDto> questions = getQuestions(feedbackDto.getAnswers());
        questions.forEach(question ->{
            Paragraph paragraph = new Paragraph(question.getQuestionText(),subFont);
            List list = new List();
            feedbackDto.getAnswers().forEach(answer ->{
                if(answer.getQuestion().getQuestionText().equals(question.getQuestionText()))
                    list.add(answer.getText());
            });
            try {
                document.add(paragraph);
                document.add(list);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        });
        document.close();
    }

    private static Set<QuestionDto> getQuestions(Set<AnswerDto> answers){
        Comparator<QuestionDto> comparator = (q1,q2) ->(q1.getQuestionText().compareTo(q2.getQuestionText()));
        Set<QuestionDto> questions = new TreeSet<>(comparator);
        answers.forEach(answer-> {
            questions.add(answer.getQuestion());
        });
        return questions;
    }
}
