package feedy.config.spring;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import feedy.bootstrap.Loader;
import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.repositories.RoleRepository;
import feedy.repositories.TeamRepository;
import feedy.repositories.UserRepository;
import feedy.services.RoleServiceImpl;
import feedy.services.TeamServiceImpl;
import feedy.services.UserServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JUnitSpringConfig.class })
@SpringBootApplication
@WebAppConfiguration
@DirtiesContext
public class SpringConfigTest {

	Loader loader;

	@Autowired
	UserRepository userRepository;
	@Autowired
	TeamRepository teamRepository;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	UserServiceImpl userService;
	@Autowired
	RoleServiceImpl roleService;
	@Autowired
	TeamServiceImpl teamService;

	Team java;

	Team dotNET;

	Role userRole;

	Role sulRole;

	private static final String JHON_SMITH = "Jhon Smith";

	private static final String ADMIN = "admin";

	private static final String DUMMY = "dummy";

	@Before
	public void setUp() {
		java = new Team("Java", null);
		dotNET = new Team(".NET", null);
		userRole = new Role("user");
		sulRole = new Role("SUL");
		roleService.saveOrUpdate(sulRole);
		teamService.saveOrUpdate(java);
	}

	@Test
	public void testUserAutowireCapabilityForJUnit() {
		assertNotNull(userService);
	}

	@Test
	public void testRoleAutowireCapabilityForJUnit() {
		assertNotNull(roleService);
	}

	@Test
	public void testTeamAutowireCapabilityForJUnit() {
		assertNotNull(teamService);
	}

	@Test
	public void testUserAutowireCapabilityForRepo() {
		assertNotNull(userRepository);
	}

	@Test
	public void testRoleAutowireCapabilityForRepo() {
		assertNotNull(roleRepository);
	}

	@Test
	public void testTeamAutowireCapabilityForRepo() {
		assertNotNull(teamRepository);
	}

	@Test
	public void testSaveUser() {

		User user1 = new User(JHON_SMITH, ADMIN, sulRole, java);
		userService.saveOrUpdate(user1);
		Integer id = user1.getId();
		User userSaved = userService.getById(id);
		assertTrue(userSaved.equals(user1));
	}

	@Test
	public void testUpdateUserName() {

		User user = new User(JHON_SMITH, ADMIN, sulRole, java);
		userService.saveOrUpdate(user);
		Integer id = user.getId();
		user.setUsername("Jhon Smmith");
		userService.saveOrUpdate(user);
		User userUpdated = userService.getById(id);
		assertTrue("Jhon Smmith".equals(userUpdated.getUsername()));

	}

	@Test
	public void testRoleEquality() {
		Role r = new Role("SUL");
		assertTrue(sulRole.equals(r));
	}

	@Test
	public void testTeamEquality() {
		Team team = new Team("Java", null);
		assertTrue(team.equals(java));
	}

	@Test
	public void testUpdateUserPassword() {

		User user = new User(JHON_SMITH, ADMIN, sulRole, java);
		userService.saveOrUpdate(user);
		Integer id = user.getId();
		user.setPassword("somepass");
		userService.saveOrUpdate(user);
		User userUpdated = userService.getById(id);
		assertTrue("somepass".equals(userUpdated.getPassword()));

	}

	@Test
	public void testDeleteUser() {

		User user = new User(JHON_SMITH, ADMIN, sulRole, java);
		userService.saveOrUpdate(user);
		userService.delete(1);
		user = userService.getById(1);
		assertNull(user);
	}

	@Test
	public void testDeleteRole() {
		Role role = new Role(DUMMY);
		roleService.saveOrUpdate(role);
		Integer id = role.getId();
		roleService.delete(id);
		role = roleService.getById(id);
		assertNull(role);
	}

	@Test
	public void testDeleteTeam() {
		Team team = new Team(DUMMY, null);
		teamService.saveOrUpdate(team);
		Integer id = team.getId();
		teamService.delete(id);
		assertNull(teamService.getById(id));
	}

	@Test
	public void testUpdateRole() {
		Role role = new Role(DUMMY);
		roleService.saveOrUpdate(role);
		Integer id = role.getId();
		role.setName(DUMMY);
		roleService.saveOrUpdate(role);
		Role updated = roleService.getById(id);
		assertTrue(updated.getName().equals(role.getName()));
	}

	@Test
	public void testUpdateTeam() {
		Team team = new Team(DUMMY, null);
		teamService.saveOrUpdate(team);
		Integer id = team.getId();
		team.setName(DUMMY);
		teamService.saveOrUpdate(team);
		Team updated = teamService.getById(id);
		assertTrue(updated.getName().equals(team.getName()));
	}

}
