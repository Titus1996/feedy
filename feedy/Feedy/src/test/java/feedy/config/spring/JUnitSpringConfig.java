package feedy.config.spring;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableWebSecurity
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EntityScan(basePackages = "feedy.domain")
@EnableJpaRepositories(basePackages = "feedy.repositories")

@ComponentScan(basePackages = { "feedy" }, //
		excludeFilters = @ComponentScan.Filter(type = FilterType.ASPECTJ, pattern = "feedy.config.spring.*"))

public class JUnitSpringConfig {

}
