package feedy.security.test;

import static org.junit.Assert.fail;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import feedy.FeedyApplication;
import feedy.configuration.SecurityConfiguration;
import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.repositories.RoleRepository;
import feedy.repositories.TeamRepository;
import feedy.repositories.UserRepository;
import feedy.security.other.AccountCredentials;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { FeedyApplication.class,
		SecurityConfiguration.class })
public class LoginTest {

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private WebApplicationContext context;

	private static final String ADMIN = "admin";

	private static final Logger LOGGER = Logger.getLogger(LoginTest.class.getName());
	private MockMvc mockMvc;

	public static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);

		} catch (Exception e) {
			fail();
			return null;
		}
	}

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		Team java = new Team("Java", null);
		Team dotNET = new Team(".NET", null);
		Role userRole = new Role("user");
		Role sulRole = new Role("SUL");
		roleRepository.save(userRole);
		roleRepository.save(sulRole);
		teamRepository.save(java);
		teamRepository.save(dotNET);

		// sul team 1
		User user1 = new User(ADMIN, ADMIN, sulRole, java);
		userRepository.save(user1);

		User user2 = new User("George McCollin", "123456", userRole, java);
		userRepository.save(user2);

		User user3 = new User("Hannah Silver", "000000", userRole, java);
		userRepository.save(user3);

		User user4 = new User("Alaric Saltzman", "111111", userRole, java);
		userRepository.save(user4);

		// sul team 2
		User user5 = new User("Debbie Frost", "222222", sulRole, dotNET);
		userRepository.save(user5);

		User user6 = new User("Rebecca Beckam", "333333", userRole, dotNET);
		userRepository.save(user6);

		User user7 = new User("David Fisch", "444444", userRole, dotNET);
		userRepository.save(user7);
	}

	@Test
	public void testLogin() {
		AccountCredentials creds = new AccountCredentials();
		creds.setUsername("Debbie Frost");
		creds.setPassword("222222");
		try {
			mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(asJsonString(creds)))
					.andExpect(status().isOk());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "testLogin", e);
		}
	}

	@Test
	public void testBadLogin() {
		AccountCredentials creds = new AccountCredentials();
		creds.setUsername("NOTadmin");
		creds.setPassword("NOTadmin");
		try {
			mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(asJsonString(creds)))
					.andExpect(status().isUnauthorized());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "testBadLogin", e);
		}
	}

}
