package feedy.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import feedy.FeedyApplication;
import feedy.domain.Answer;
import feedy.domain.Feedback;
import feedy.domain.Question;
import feedy.domain.QuestionType;
import feedy.domain.ResponseChoice;
import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.Template;
import feedy.domain.User;
import feedy.repositories.FeedbackRepository;
import feedy.repositories.QuestionTypeRepository;
import feedy.repositories.RoleRepository;
import feedy.repositories.TeamRepository;
import feedy.repositories.TemplateRepository;
import feedy.repositories.UserRepository;
import feedy.services.FeedbackService;
import feedy.services.QuestionService;
import feedy.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { FeedyApplication.class })
public class ReportsControllerTest {

	private static final String PROCENTAGE_URL = "/getProcentageForFeedback";
	private static final String TOPANSWERS_URL = "/getTopAnswersForTemplateQuestions";
	private static final String TEMPLATESBYUSAGE_URL = "/getTemplatesByUsage";
	private static final Logger LOGGER = Logger.getLogger(ReportsControllerTest.class.getName());

	private MockMvc mockMvc;

	@Autowired
	private FeedbackService feedbackService;
	
	@Autowired
	private WebApplicationContext context;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TemplateRepository templateRepository;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private FeedbackRepository feedbackRepository;

	@Autowired
	private QuestionTypeRepository questionTypeRepository;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

		QuestionType singleChoiceQuestionType = new QuestionType("SINGLE_CHOICE");
		QuestionType multipleChoiceQuestionType = new QuestionType("MULTIPLE_CHOICE");
		QuestionType freetextQuestionType = new QuestionType("FREETEXT");

		questionTypeRepository.save(singleChoiceQuestionType);
		questionTypeRepository.save(multipleChoiceQuestionType);
		questionTypeRepository.save(freetextQuestionType);

		///////////////////// TEAM,ROLE

		Team java = new Team("Java", null);
		Team dotNET = new Team(".NET", null);

		Role userRole = new Role("Normal");
		Role sulRole = new Role("SUL");

		roleRepository.save(userRole);
		roleRepository.save(sulRole);
		teamRepository.save(java);
		teamRepository.save(dotNET);

		///////////// TEMPLATES

		Template templateExample = new Template("BasicQuestions", "Jhon Smith", null);
		Template templateSecondExample = new Template("QuestionsSUL", "Hannah Silver", null);
		templateRepository.save(templateExample);
		templateRepository.save(templateSecondExample);

		//////////// ResponseChoices

		///////////////// single1

		ResponseChoice responseChoiceSingleChoiceQuestion11 = new ResponseChoice("bad");
		ResponseChoice responseChoiceSingleChoiceQuestion12 = new ResponseChoice("ok");
		ResponseChoice responseChoiceSingleChoiceQuestion13 = new ResponseChoice("good");
		ResponseChoice responseChoiceSingleChoiceQuestion14 = new ResponseChoice("excellent");

		Set<ResponseChoice> respChoiceSetSingle1 = new HashSet<>();
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion11);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion12);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion13);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion14);

		///////////////////////// multiple1
		ResponseChoice responseChoiceMultipleChoiceQuestion11 = new ResponseChoice("sociable");
		ResponseChoice responseChoiceMultipleChoiceQuestion12 = new ResponseChoice("good listener");
		ResponseChoice responseChoiceMultipleChoiceQuestion13 = new ResponseChoice("optimistic");
		ResponseChoice responseChoiceMultipleChoiceQuestion14 = new ResponseChoice("always in time");

		Set<ResponseChoice> respChoiceSetMultiple1 = new HashSet<>();
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion11);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion12);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion13);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion14);

		/////////// single2

		ResponseChoice responseChoiceSingleChoiceQuestion21 = new ResponseChoice("never");
		ResponseChoice responseChoiceSingleChoiceQuestion22 = new ResponseChoice("sometimes");
		ResponseChoice responseChoiceSingleChoiceQuestion23 = new ResponseChoice("often");
		ResponseChoice responseChoiceSingleChoiceQuestion24 = new ResponseChoice("almost all the time");

		Set<ResponseChoice> respChoiceSetSingle2 = new HashSet<>();
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion21);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion22);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion23);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion24);

		////////////// multiple2

		ResponseChoice responseChoiceMultipleChoiceQuestion21 = new ResponseChoice("gym");
		ResponseChoice responseChoiceMultipleChoiceQuestion22 = new ResponseChoice("football");
		ResponseChoice responseChoiceMultipleChoiceQuestion23 = new ResponseChoice("photography");
		ResponseChoice responseChoiceMultipleChoiceQuestion24 = new ResponseChoice("movies");

		Set<ResponseChoice> respChoiceSetMultiple2 = new HashSet<>();
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion21);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion22);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion23);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion24);

		////////// QUESTIONS

		Question singleChoiceQuestion1 = new Question(templateExample, "how good is he/she at cooking?",
				singleChoiceQuestionType, respChoiceSetSingle1, null);

		Question multipleChoiceQuestion1 = new Question(templateExample, "what are their skills at work?",
				multipleChoiceQuestionType, respChoiceSetMultiple1, null);

		Question question1 = new Question(templateSecondExample, "Describe their attitude at work.",
				freetextQuestionType, null, null);
		Question singleChoiceQuestion2 = new Question(templateSecondExample, "Is he/she late for work?",
				singleChoiceQuestionType, respChoiceSetSingle2, null);
		Question multipleChoiceQuestion2 = new Question(templateSecondExample, "what are their hobbies?",
				multipleChoiceQuestionType, respChoiceSetMultiple2, null);

		questionService.saveOrUpdate(singleChoiceQuestion1);
		questionService.saveOrUpdate(multipleChoiceQuestion1);
		questionService.saveOrUpdate(question1);
		questionService.saveOrUpdate(singleChoiceQuestion2);
		questionService.saveOrUpdate(multipleChoiceQuestion2);

		Set<Question> questionSet = new HashSet<>();

		questionSet.add(singleChoiceQuestion1);
		questionSet.add(multipleChoiceQuestion1);

		Set<Question> questionSet1 = new HashSet<>();
		questionSet1.add(question1);
		questionSet1.add(singleChoiceQuestion2);
		questionSet1.add(multipleChoiceQuestion2);

		templateExample.setQuestions(questionSet);
		templateRepository.save(templateExample);

		templateSecondExample.setQuestions(questionSet1);
		templateRepository.save(templateSecondExample);
		/////////// ANSWERS
		Answer answer = new Answer("a", singleChoiceQuestion1);
		Answer answer2 = new Answer("a", singleChoiceQuestion1);
		Answer answer3 = new Answer("b", singleChoiceQuestion1);
		Answer answer4 = new Answer("b", singleChoiceQuestion1);
		Answer answer5 = new Answer("c", singleChoiceQuestion1);
		Answer answer6 = new Answer("c", singleChoiceQuestion1);
		Answer answer7 = new Answer("d", singleChoiceQuestion1);
		//////////////////// USERS

		User javaUserSUL = new User("Jhon Smith", "admin", sulRole, java);
		userRepository.save(javaUserSUL);

		User javaUserNormal1 = new User("George McCollin", "123456", userRole, java);
		userRepository.save(javaUserNormal1);

		User javaUserNormal2 = new User("Hannah Silver", "000000", userRole, java);
		userRepository.save(javaUserNormal2);

		User javaUserNormal3 = new User("Alaric Saltzman", "111111", userRole, java);
		userRepository.save(javaUserNormal3);

		User dotNETUserSUL = new User("Debbie Frost", "222222", sulRole, dotNET);
		userRepository.save(dotNETUserSUL);

		User dotNETUserNormal1 = new User("Rebecca Beckam", "333333", userRole, dotNET);
		userRepository.save(dotNETUserNormal1);

		User dotNETUserNormal2 = new User("David Fisch", "444444", userRole, dotNET);
		userRepository.save(dotNETUserNormal2);

		// Feedbacks
		Date date = new Date(0);
		Date date1 = new Date(0);

		Set<Answer> answers = new TreeSet<>(Comparator.comparing(Answer::getText));
		answers.add(answer);
		answers.add(answer2);
		answers.add(answer3);
		answers.add(answer4);
		answers.add(answer5);
		answers.add(answer6);
		answers.add(answer7);
		/*Feedback feedback1 = new Feedback(javaUserSUL, javaUserNormal3, false, date, answers);
		Feedback feedback2 = new Feedback(javaUserSUL, dotNETUserSUL, false, date1, new TreeSet<>());
		feedbackRepository.save(feedback1);
		feedbackRepository.save(feedback2);

		questionService.listAll();*/

	}

	@Test
	@WithMockUser(roles = "SUL")
	public void testProcentage() {
		feedbackService.listAll();
		userService.listAll();
		try {
			this.mockMvc.perform(get(PROCENTAGE_URL + "/" + 1)).andExpect(status().isOk());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "testProcentage", e);
		}
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void testAnswers() {
		feedbackService.listAll();
		try {
			this.mockMvc.perform(get(TOPANSWERS_URL + "/" + 1)).andExpect(status().isOk()).andDo(print());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "testAnswers", e);
		}
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void testTempaltesByUsage() {
		try {
			 this.mockMvc.perform(get(TEMPLATESBYUSAGE_URL).content("{\"BasicQuestions\":1,\"QuestionsSUL\":0}")).andExpect(status().isOk());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "testTemplatesByUsage", e);
		}
	}

}
