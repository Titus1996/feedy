package feedy.controllers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Set;
import java.util.TreeSet;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import feedy.FeedyApplication;
import feedy.domain.Question;
import feedy.domain.QuestionType;
import feedy.domain.ResponseChoice;
import feedy.dtos.QuestionDto;
import feedy.dtos.ResponseChoiceDto;
import feedy.services.QuestionService;
import feedy.services.QuestionTypeService;
import feedy.services.ResponseChoiceService;

@RunWith(SpringJUnit4ClassRunner.class)

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { FeedyApplication.class })
public class QuestionControllerTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private static final String GET_RESPONSES_URL = "/responseChoiceByQuestion";
	private static final String SAVE_URL = "/saveQuestion";
	private static final Logger LOGGER = Logger.getLogger( UserControllerTest.class.getName() );
	@Autowired
	private WebApplicationContext context;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private ResponseChoiceService responseChoiceService;

	@Autowired
	private QuestionTypeService questionTypeService;

	private MockMvc mockMvc;

	private ObjectWriter ow;

	private String requestJson;

	@Before
	public void setUp() {	
		ObjectMapper mapper;
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		mapper = new ObjectMapper();
		ow = mapper.writer().withDefaultPrettyPrinter();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		requestJson = null;
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void saveQuestionTest() {
		Set<ResponseChoiceDto> responseChoicesDto = new TreeSet<>();
		QuestionType qType = new QuestionType("type");
		questionTypeService.saveOrUpdate(qType);
		QuestionDto question = new QuestionDto(1, "text", "remark", "type", responseChoicesDto);
		int beforeLength = 0;
		try {
			requestJson = ow.writeValueAsString(question);
		} catch (JsonProcessingException e1) {
			 LOGGER.log(Level.FINE,"saveQuestionTest writeValueAsString", e1);
		}
		try {
			mockMvc.perform(post(SAVE_URL).contentType(APPLICATION_JSON_UTF8).content(requestJson))
					.andExpect(status().isOk());
			assertTrue(beforeLength < questionService.listAll().size());
		} catch (Exception e) {
			 LOGGER.log(Level.FINE,"saveQuestionTest", e);
		}
	}

	@Test
	@WithMockUser(roles = "Normal,SUL")
	public void getResponseChoiceByQuestionTest() {
		Set<ResponseChoice> responseChoices = new TreeSet<>();
		QuestionType qType = new QuestionType("type");
		questionTypeService.saveOrUpdate(qType);
		Question question = new Question("text", qType, "remark", null);
		questionService.saveOrUpdate(question);
		int questionId = question.getId();
		final String TEXT_FIRST_CHOICE = "choice1";
		final String TEXT_SECOND_CHOICE = "choice2";
		final String TEXT_THIRD_CHOICE = "choice3";
		ResponseChoice r1 = new ResponseChoice("choice1");
		ResponseChoice r2 = new ResponseChoice("choice2");
		ResponseChoice r3 = new ResponseChoice("choice3");
		r1.setQuestion(question);
		r2.setQuestion(question);
		r3.setQuestion(question);
		responseChoiceService.saveOrUpdate(r1);
		responseChoiceService.saveOrUpdate(r2);
		responseChoiceService.saveOrUpdate(r3);
		responseChoices.add(r1);
		responseChoices.add(r2);
		responseChoices.add(r3);
		question.setResponseChoices(responseChoices);
		questionService.saveOrUpdate(question);
		try {
			mockMvc.perform(get(GET_RESPONSES_URL + "/" + questionId)).andExpect(status().isOk())
					.andExpect(content().contentType(APPLICATION_JSON_UTF8))
					.andExpect(jsonPath("$[0].responseChoiceText", is(TEXT_FIRST_CHOICE)))
					.andExpect(jsonPath("$[1].responseChoiceText", is(TEXT_SECOND_CHOICE)))
					.andExpect(jsonPath("$[2].responseChoiceText", is(TEXT_THIRD_CHOICE)));
		} catch (Exception e) {
			LOGGER.log(Level.FINE,"getResponseChoiceByQuestionTest", e);
		}
	}
	
	@Test
	@WithMockUser(roles = "Normal,SUL")
	public void getEmptyResponseChoiceByQuestionTest() {
		Set<ResponseChoice> responseChoices = new TreeSet<>();
		QuestionType qType = new QuestionType("type");
		questionTypeService.saveOrUpdate(qType);
		Question question = new Question("text", qType, "remark", null);
		questionService.saveOrUpdate(question);
		int questionId = question.getId();
		question.setResponseChoices(responseChoices);
		questionService.saveOrUpdate(question);
		try {
			mockMvc.perform(get(GET_RESPONSES_URL + "/" + questionId)).andExpect(status().isOk())
					.andExpect(content().contentType(APPLICATION_JSON_UTF8))
					.andExpect(jsonPath("$", IsEmptyCollection.empty()));
		} catch (Exception e) {
			LOGGER.log(Level.FINE,"getEmptyResponseChoiceByQuestionTest", e);
		}
	}
	
}
