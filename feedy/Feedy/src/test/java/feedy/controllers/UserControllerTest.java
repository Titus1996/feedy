package feedy.controllers;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import feedy.FeedyApplication;
import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.dtos.UserDto;
import feedy.services.RoleServiceImpl;
import feedy.services.TeamServiceImpl;
import feedy.services.UserServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { FeedyApplication.class })
public class UserControllerTest {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private static final String SAVE_URL = "/saveUser";
	private static final String DELETE_URL = "/deleteUser";
	private static final Logger LOGGER = Logger.getLogger(UserControllerTest.class.getName());
	@Autowired
	private UserServiceImpl userService;
	@Autowired
	RoleServiceImpl roleService;
	@Autowired
	TeamServiceImpl teamService;
	@Autowired
	UserController testunit;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	private Team dotNET;

	private Role sulRole;

	private ObjectWriter ow;

	private String requestJson;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		Team java = new Team("Java", null);
		dotNET = new Team(".NET", null);
		Role userRole = new Role("user");
		sulRole = new Role("SUL");
		User u1 = new User("Jhon Smith", "admin", sulRole, java);
		User u2 = new User("George McCollin", "123456", userRole, java);
		User u3 = new User("Debbie Frost", "222222", sulRole, dotNET);
		User u4 = new User("Rebecca Beckam", "333333", userRole, dotNET);
		roleService.saveOrUpdate(userRole);
		roleService.saveOrUpdate(sulRole);
		teamService.saveOrUpdate(java);
		teamService.saveOrUpdate(dotNET);
		userService.saveOrUpdate(u1);
		userService.saveOrUpdate(u2);
		userService.saveOrUpdate(u3);
		userService.saveOrUpdate(u4);
		ObjectMapper mapper = new ObjectMapper();
		ow = mapper.writer().withDefaultPrettyPrinter();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		requestJson = null;
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void deleteTest() {
		int id = 1;
		int beforeLength = userService.listAll().size();

		try {
			mockMvc.perform(delete(DELETE_URL + "/" + id).contentType(APPLICATION_JSON_UTF8))
					.andExpect(status().isOk());
			assertTrue(userService.listAll().size() < beforeLength);
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "delete user test", e);
		}
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void addTest() {
		User userToSave = new User("Debbie Frost", "222222", sulRole, dotNET);
		int beforeLength = userService.listAll().size();
		UserDto userDto = new UserDto(userToSave.getUsername(), userToSave.getTeam().getName(),
				userToSave.getRole().getName());

		try {
			requestJson = ow.writeValueAsString(userDto);
		} catch (JsonProcessingException e1) {
			LOGGER.log(Level.FINE, "addTest writeValueAsString", e1);
		}
		try {
			mockMvc.perform(post(SAVE_URL).contentType(APPLICATION_JSON_UTF8).content(requestJson))
					.andExpect(status().isOk());
			assertTrue(beforeLength < userService.listAll().size());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "add user test", e);
		}
	}

	/*
	 * @Test
	 * 
	 * @WithMockUser(roles = "SUL") public void editTest() { User userToEdit =
	 * userService.findByUserName("Debbie Frost").get(0); UserDto userDto = new
	 * UserDto(userToEdit.getId(), userToEdit.getName(),
	 * userToEdit.getTeam().getName(), userToEdit.getRole().getName()); int id =
	 * userToEdit.getId();
	 * 
	 * userDto.setName("Sir Debbie Frost"); try {
	 * .andExpect(jsonPath("$[0].getTeamName", is("Java")))
	 * .andExpect(jsonPath("$[1].getTeamName", is(".NET"))); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * }
	 */
}
