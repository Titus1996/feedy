package feedy.controllers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.sql.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.SerializationFeature;

import feedy.FeedyApplication;
import feedy.domain.Answer;
import feedy.domain.Feedback;
import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.User;

import feedy.services.FeedbackService;
import feedy.services.RoleService;
import feedy.services.TeamService;
import feedy.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { FeedyApplication.class })
public class FeedbackControllerTest {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private static final String SAVE_URL = "/saveFeedback";
	private static final String GET_SENT_URL = "/getSentFeedbacks";
	private static final String GET_RECEIVED_URL = "/getReceivedFeedbacks";
	private static final Logger LOGGER = Logger.getLogger(UserControllerTest.class.getName());

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private UserService userService;

	private MockMvc mockMvc;

	private String requestJson;

	private static final String PAUL_POP = "Paul Pop";

	private static final String TITUS_BOCIOC = "Titus Bocioc";

	@Before
	public void setUp() {
		ObjectMapper mapper;

		Set<Answer> answers = new TreeSet<>();
		Team java = new Team("Java", null);
		Team dotNET = new Team(".NET", null);
		Role userRole = new Role("Normal");
		Role sulRole = new Role("SUL");
		User user5 = new User(PAUL_POP, "123456", userRole, java);
		User user6 = new User(TITUS_BOCIOC, "789123", sulRole, dotNET);
		User user7 = new User("Denisa Bud", "encrypted", userRole, java);

		roleService.saveOrUpdate(userRole);
		roleService.saveOrUpdate(sulRole);
		teamService.saveOrUpdate(java);
		teamService.saveOrUpdate(dotNET);
		userService.saveOrUpdate(user5);
		userService.saveOrUpdate(user6);
		userService.saveOrUpdate(user7);
		Date date = new Date(0);
		Date date1 = new Date(0);
		/*Feedback feedback1 = new Feedback(user5, user6, false, date, answers);
		Feedback feedback2 = new Feedback(user5, user7, false, date1, answers);
		feedbackService.saveOrUpdate(feedback1);
		feedbackService.saveOrUpdate(feedback2);
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		mapper = new ObjectMapper();

		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		requestJson = null;*/
	}

	@Test
	@WithMockUser(roles = "Normal,SUL")
	public void saveFeedbackTest() {

		Team java = new Team("Java", null);
		Team dotNET = new Team(".NET", null);
		Role userRole = new Role("Normal");
		Role sulRole = new Role("SUL");
		User user5 = new User(PAUL_POP, "123456", userRole, java);
		User user6 = new User(TITUS_BOCIOC, "789123", sulRole, dotNET);

		roleService.saveOrUpdate(userRole);
		roleService.saveOrUpdate(sulRole);
		teamService.saveOrUpdate(java);
		teamService.saveOrUpdate(dotNET);
		userService.saveOrUpdate(user5);
		userService.saveOrUpdate(user6);

		int beforeLength = 0;

		try {
			mockMvc.perform(post(SAVE_URL).contentType(APPLICATION_JSON_UTF8).content(requestJson))
					.andExpect(status().isOk());
			assertTrue(beforeLength < feedbackService.listAll().size());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "saveFeedbackTest", e);
		}
	}

	@Test
	@WithMockUser(roles = "Normal,SUL")
	public void getSentFeedbacksTest() {
		final String FROM = PAUL_POP;
		try {
			mockMvc.perform(get(GET_SENT_URL + "/" + FROM)).andExpect(status().isOk())
					.andExpect(content().contentType(APPLICATION_JSON_UTF8)).andExpect(jsonPath("$[0].from", is(FROM)))
					.andExpect(jsonPath("$[1].from", is(FROM)));

		} catch (Exception e) {
			LOGGER.log(Level.FINE, "getTemplateByNameTest", e);
		}
	}

	@Test
	@WithMockUser(roles = "Normal,SUL")
	public void getReceivedFeedbacksTest() {
		final String TO = TITUS_BOCIOC;
		try {
			mockMvc.perform(get(GET_RECEIVED_URL + "/" + TO)).andExpect(status().isOk())
					.andExpect(content().contentType(APPLICATION_JSON_UTF8)).andExpect(jsonPath("$[0].to", is(TO)));
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "getTemplateByNameTest", e);
		}
	}
}
