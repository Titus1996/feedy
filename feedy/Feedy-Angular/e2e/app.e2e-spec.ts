import { FeedyAngularPage } from './app.po';

describe('feedy-angular App', () => {
  let page: FeedyAngularPage;

  beforeEach(() => {
    page = new FeedyAngularPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
