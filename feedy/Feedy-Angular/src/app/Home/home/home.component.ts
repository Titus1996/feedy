import { LoginService } from '../../Shared/Services/login.service';
import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { LoginComponent } from 'app/Home/home/login/login.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  username: string;
  password: string;

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
  }

  login() {
    let disposable = this.dialog.open(LoginComponent, {
      width: '20vw',
      panelClass:'contaner-login'
    });
  
  }

}
