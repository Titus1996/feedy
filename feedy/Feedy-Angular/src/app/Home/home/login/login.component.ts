
import {Component, HostListener, OnInit, Inject} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {LoginService} from '../../../Shared/Services/login.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{
  username =  '';
  password =  '';
  errorMessage = false;

  constructor(private loginService: LoginService, public dialogRef: MatDialogRef<LoginComponent>, private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }
  login() {
    if(this.validatePassword() !== 0 || this.validateUsername() !==0){
      return;
    }
    this.loginService.login(this.username, this.password).subscribe(res => {
      this.router.navigate(['/colleagues']);
      this.dialogRef.close();
    },
    err=>{
      this.errorMessage = true;
    });

  }

  validatePassword(){
    if(this.password.length === 0 ){
      return 1;
    }
    if(this.password.length> 0 && this.password.length < 4){
      return 2;
    }
    return 0;
  }

  validateUsername(){
    if(this.username.length === 0 ){
      return 1;
    }
    return 0;
  }

}
