import {LoginService} from './Shared/Services/login.service';
import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private loginService: LoginService) {

  }

  public isLoggedIn(): boolean {
    return this.loginService.isLoggedIn();
  }

  public getRole(): string {
    return this.loginService.getRole();
  }
}
