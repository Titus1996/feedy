import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { Template } from 'app/Shared/Model/template';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { TemplateService } from 'app/Shared/Services/template.service';
import { Question } from 'app/Shared/Model/question';
import { ResponseChoice } from 'app/Shared/Model/ResponseChoice';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-edit-template',
  templateUrl: './edit-template.component.html',
  styleUrls: ['./edit-template.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class EditTemplateComponent  implements OnInit {
  template: Template;
  public myForm: FormGroup;
  public questionTypes: string[];
  public defaultType: string;

  getFormQuestions() {
    return (<FormGroup>this.myForm.get('questions')).controls;
  }

  constructor(private _fb: FormBuilder, private templateService: TemplateService, private dialogRef: MatDialogRef<EditTemplateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.template = data.template;
    this.questionTypes = ['SINGLE_CHOICE', 'MULTIPLE_CHOICE', 'FREETEXT', 'RATING'];
    this.defaultType = 'FREETEXT';
  }

  ngOnInit() {
    this.template.questions.sort((q1,q2)=>q1.id-q2.id);
    this.myForm = this._fb.group({
      name:  [ this.template.name, [Validators.required, Validators.minLength(5)]],
      isGeneric: [this.template.isGeneric],
      questions: this._fb.array([])
    });
    this.loadQuestions(this.template.questions);
  }

  initQuestion() {
    return this._fb.group({
      id: [0],
      questionText: ['', Validators.required],
      questionType: [this.defaultType, [Validators.required]],
      remark: [''],     
      min:[''],
      max:[''],
      required:[''],
      responseChoices: this._fb.array([])
    });
  }

  convertQuestion(question: Question) {
    return this._fb.group({
      id: [question.id],
      questionText: [question.questionText, Validators.required],
      questionType: [question.questionType, [Validators.required]],
      remark: [question.remark],
      min:[question.min],
      max:[question.max],
      required:[question.required],
      responseChoices: this.loadResponses(question.responseChoices)
    });
  }

  loadResponses(responses: ResponseChoice[]) {
      let responseArray = this._fb.array([]);
      responses.forEach(response => {
        responseArray.push(this.createResponse(response));
      });
      return responseArray;
  }

  createResponse(responseChoice: ResponseChoice){
    return this._fb.group({
      id: responseChoice.id,
      responseChoiceText: [responseChoice.responseChoiceText, Validators.required],
    });
  }

  loadQuestions(questions: Question[]) {
      const questionControl = <FormArray>this.myForm.controls['questions'];
      questions.forEach(question => {
        questionControl.push(this.convertQuestion(question));
      })
  }

  clearArray(formArray: FormArray) {
    let length = formArray.length;
    while (length > 0) {
      formArray.removeAt(length - 1);
      length = length - 1;
    }
    formArray.removeAt(0);
  }

  addQuestion() {
    const questionControl = <FormArray>this.myForm.controls['questions'];
    let question = this.initQuestion();
    question['controls']['questionType']['valueChanges'].subscribe(val => {
      if (question['controls']['questionType']['value'] === 'FREETEXT') {
        this.clearArray(<FormArray>question['controls']['responseChoices']);
      }
    });
    questionControl.push(question);
  }
  removeQuestion(i: number) {
    const questionControl = <FormArray>this.myForm.controls['questions'];
    questionControl.removeAt(i);
  }

  initAnswer() {
    return this._fb.group({
      id: [0],
      responseChoiceText: ['', Validators.required],
    });
  }

  removeAnswer(i: number, j: number) {
    const answerControl = this.myForm.controls.questions['controls'][i]['controls'].responseChoices;
    answerControl.removeAt(j);
  }

  addAnswer(i: number) {
    let answerControl = this.myForm.controls.questions['controls'][i]['controls'].responseChoices;
    let answer = this.initAnswer();
    answerControl.push(answer);
  }

  getAnswers(i) {
    return this.myForm.controls.questions['controls'][i]['controls'].responseChoices.controls;
  }

  getQuestionType(i) {
    let value = this.myForm.controls.questions['controls'][i]['controls'].questionType.value;
    
    //console.log(value);
    if (value === '') {
      return this.defaultType;
    }
    return value;
  }

  validateTempalte(template: Template): boolean {
    let i, j;
    let valid = true;
    for ( i = 0; i < template.questions.length; i++) {
      if (template.questions[i].questionText === '') {
        valid = false;
      }
      for ( j = 0; j < template.questions[i].responseChoices.length; j++) {
        if ( template.questions[i].responseChoices[j].responseChoiceText === '') {
          valid = false;
        }
      }
    }
    return valid;
  }

  save(template: Template) {
    console.log(template);
    template.id = this.template.id;
    if (this.validateTempalte(template) === true) {
      let noError = true;
      this.templateService.saveTemplate(template).subscribe();
      this.dialogRef.close();
    }
  }


  restrictTwoAnswers(index: number) {
    let length = this.getAnswers(index).length;
    let newQuestion = true;
    if (length < 2 && length >= 0) {
      if (length > 0) {
        newQuestion = false;
      }
      while (length < 2) {
        this.addAnswer(index);
        length++;
      }
    }
    return newQuestion;
  }
}
