import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {FormGroup, FormArray, FormBuilder, Validators} from '@angular/forms';
import {Template} from '../../Shared/Model/template';
import {TemplateService} from '../../Shared/Services/template.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-template-generator',
  templateUrl: './template-generator.component.html',
  styleUrls: ['./template-generator.component.css']
})

export class TemplateGeneratorComponent implements OnInit {

  public myForm: FormGroup;
  public questionTypes: string[];
  public defaultType: string;
  public successfulSave: boolean = false;


  constructor(private _fb: FormBuilder, private templateService: TemplateService, private toastr: ToastrService, private router: Router, private cdRef : ChangeDetectorRef) {
    this.questionTypes = ['FREETEXT','SINGLE_CHOICE', 'MULTIPLE_CHOICE', 'RATING'];
    this.defaultType = 'FREETEXT';
  }

  navigateToViewTemplates(){
    this.router.navigate(['templates'])
  }

  ngOnInit() {
    this.myForm = this._fb.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      isGeneric: [''],
      questions: this._fb.array([])
    });
    this.addQuestion();
  }

  clearArray(formArray: FormArray) {
    let length = formArray.length;
    while (length > 0) {
      formArray.removeAt(length - 1);
      length = length - 1;
    }
    formArray.removeAt(0);
  }

  addQuestion() {
    const questionControl = <FormArray>this.myForm.controls['questions'];
    let question = this.initQuestion();
    question['controls']['questionType']['valueChanges'].subscribe(val => {
      let resArray = <FormArray>question['controls']['responseChoices'];
      this.clearArray(resArray);
      if(val !== this.defaultType && val !== 'RATING'){
          resArray.push(this.initAnswer());
          resArray.push(this.initAnswer());
      }
    });
    questionControl.push(question);
  }

  removeQuestion(i: number) {
    const questionControl = <FormArray>this.myForm.controls['questions'];
    questionControl.removeAt(i);
  }

  initQuestion() {
    return this._fb.group({
      questionText: ['', Validators.required],
      questionType: [this.defaultType, [Validators.required]],
      remark: [''],
      min:[''],
      max:[''],
      isRequired:[''],
      responseChoices: this._fb.array([])
    });
  }

  initAnswer() {
    return this._fb.group({
      responseChoiceText: ['', Validators.required],
    });
  }

  removeAnswer(i: number, j: number) {
    const answerControl = this.myForm.controls.questions['controls'][i]['controls'].responseChoices;
    answerControl.removeAt(j);
  }

  addAnswer(i: number) {
    let answerControl = this.myForm.controls.questions['controls'][i]['controls'].responseChoices;
    let answer = this.initAnswer();
    answerControl.push(answer);
  }

  removeRemark(i) {
    this.myForm.controls.questions['controls'][i]['controls'].remark.reset();
  }

  getAnswers(i) {
    return this.myForm.controls.questions['controls'][i]['controls'].responseChoices.controls;
  }

  getQuestionType(i) {
    let value = this.myForm.controls.questions['controls'][i]['controls'].questionType.value;
    if (value === '' || value === undefined) {
      return this.defaultType;
    }
    return value;
  }

  successToast() {
    this.toastr.success('Template Added!', 'Success!');
  }

  failureToast(error:string,header:string) {
    this.toastr.error(error, header);
  }

  validateTemplate(template: Template): boolean {
    let i, j;
    let valid = true;
    for (i = 0; i < template.questions.length; i++) {
      if (template.questions[i].questionText === '') {
        valid = false;
      }
      for (j = 0; j < template.questions[i].responseChoices.length; j++) {
        if (template.questions[i].responseChoices[j].responseChoiceText === '') {
          console.log('Crapa1');
          valid = false;
        }
      }
    }
    return valid;
  }

  save(template: Template) {
    if (this.validateTemplate(template) === true) {
      this.templateService.saveTemplate(template).subscribe(res => {
          this.successToast();
          this.clearArray(<FormArray>this.myForm.controls['questions']);
          this.myForm.reset();
          this.ngOnInit();
          this.successfulSave = true;
        },
        err => {
          this.failureToast("Server error","Error");
        });
    }else{
      this.failureToast("Invalid Template","Invalid");
    }
  }

  restrictTwoAnswers(index: number) {
    let length = this.getAnswers(index).length;
    let newQuestion = true;
    if (length < 2 && length >= 0) {
      if (length > 0) {
        newQuestion = false;
      }
      while (length < 2) {
        this.addAnswer(index);
        length++;
      }
    }
    return newQuestion;
  }


}

