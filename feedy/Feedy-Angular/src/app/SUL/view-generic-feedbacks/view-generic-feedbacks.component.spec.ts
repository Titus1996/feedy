import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewGenericFeedbacksComponent } from './view-generic-feedbacks.component';

describe('ViewGenericFeedbacksComponent', () => {
  let component: ViewGenericFeedbacksComponent;
  let fixture: ComponentFixture<ViewGenericFeedbacksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGenericFeedbacksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGenericFeedbacksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
