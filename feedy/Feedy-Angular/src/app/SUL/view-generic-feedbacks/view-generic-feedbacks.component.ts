import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { FeedbackService } from 'app/Shared/Services/feedback.service';
import { Feedback } from 'app/Shared/Model/feedback';
import { FeedbackModalComponent } from 'app/Shared/Components/view-feedbacks/feedback-modal/feedback-modal.component';

@Component({
  selector: 'app-view-generic-feedbacks',
  templateUrl: './view-generic-feedbacks.component.html',
  styleUrls: ['./view-generic-feedbacks.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ViewGenericFeedbacksComponent implements OnInit {
  feedbacks:Feedback[] = [];
  dataSource: MatTableDataSource<Feedback>;
  displayedColumns = ['Template', 'from', 'date', 'view'];
  @ViewChild(MatPaginator)
  private paginator: MatPaginator;

  @ViewChild(MatSort)
  private sort: MatSort;

  constructor(private feedbackService: FeedbackService, private dialog: MatDialog) {
    this.getGenericFeedbacks();
   }

  ngOnInit() {}

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  private getGenericFeedbacks(){
    this.feedbackService.getGenericFeedbacks()
    .subscribe(feedbacks => {
      this.feedbacks = feedbacks;
      this.dataSource = new MatTableDataSource<Feedback>(this.feedbacks);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    err=>console.log(err));
  }

  deleteFeedback(feedback: Feedback){
    this.feedbackService.deleteFeedback(feedback.id).subscribe(res =>{
      if(res){
        this.getGenericFeedbacks();
      }
    });
  }

  ViewFeedback(feedback: Feedback) {
    this.dialog.open(FeedbackModalComponent, {
      data: {
        feedback: feedback,
      },
      panelClass:"feedback-wrapper",
      width: "40vw",
      height: "65vh"

    });
  }

}
