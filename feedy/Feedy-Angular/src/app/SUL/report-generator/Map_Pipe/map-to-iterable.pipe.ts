import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapToIterable'
})
export class MapToIterablePipe implements PipeTransform {

  transform(dict) {
    let a = [];
    for (let key in dict) {
        a.push(key);
    }
    return a;
  }

}
