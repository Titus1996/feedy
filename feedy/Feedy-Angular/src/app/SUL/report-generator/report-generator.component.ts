 import {Component, OnInit, Input, ChangeDetectorRef} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Template} from '../../Shared/Model/template';
import {TemplateService} from '../../Shared/Services/template.service';
import {ReportService} from '../../Shared/Services/report.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import { Output } from '@angular/core/src/metadata/directives';
import { EventEmitter } from 'selenium-webdriver';
import { Observable } from 'rxjs/Observable';
import * as jsPDF from 'jspdf';
import { Colors } from 'app/SUL/report-generator/Colors/Colors';
declare var $: any;
@Component({
  selector: 'app-report-generator',
  templateUrl: './report-generator.component.html',
  styleUrls: ['./report-generator.component.css']
})
export class ReportGeneratorComponent implements OnInit {
  selectedTemplate: number;
  public myForm: FormGroup;
  public templateTypes: string[];
  public templates: Template[];
  public procentageForTemplate: number;
  public topResultForTemplate: Map<string, Map<string, number>>[] = [];
  public showResult: boolean = false;
  public templateUsage;
  public selected:Template;
  // PolarArea
  public polarAreaChartData: number[] = [];
  public polarAreaLegend: boolean = true;
  public polarAreaChartLabels: string[] = [];
  public polarAreaChartType: string = 'pie';
  public colors = Colors.colors;
  constructor(private templateService: TemplateService, private reportService: ReportService, private toastr: ToastrService, private router: Router, private cdRef : ChangeDetectorRef) {}

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }

  ngOnInit() {
    this.templateTypes = ['Template Usage', 'Specific Templates'];
    this.setTemplates();
    this.getMostUsedTemplates();

  }

  setTemplates() {
    this.templateService.getTemplates().subscribe(templates => this.templates = templates);
  }

  getTemplateNameById(id:number){
    let template;
    this.templates.forEach(temp=>{
      if(temp.id===id){
        template=temp;
      }
    });
    if(template !== undefined){
      this.selected=template;
      console.log(this.selected);
    }
    else{
      this.showResult = false;
      this.cdRef.detectChanges();
    }
  }
  

  getProcentageForTemplate() {
    if(this.selectedTemplate === undefined || this.selectedTemplate === -1){
      return false;
    }
    else{
      this.getTemplateNameById(this.selectedTemplate);
      this.reportService.GetProcentageForFeedback(this.selectedTemplate).subscribe(procentageForTemplate => {
        this.procentageForTemplate = procentageForTemplate;
      });
      return true;
    }

  }

  getTopResultForTemplate() {
    if(this.selectedTemplate === undefined || this.selectedTemplate === -1){
      return false;
    }
    this.reportService.GetTopAnswersForTemplateQuestions(this.selectedTemplate).subscribe(topResultForTemplate => {
      this.topResultForTemplate = topResultForTemplate;
    });
  }

  getMostUsedTemplates() {

    this.reportService.GetTemplatesByUsage().subscribe(data => {
      this.templateUsage = data;
      for (let i in data) {
        this.polarAreaChartLabels.push(i);
        this.polarAreaChartData.push(data[i]);
      }
    });
  }

  setResult(showPieChart) {

    this.topResultForTemplate = [];
    this.getTopResultForTemplate();
    this.showResult = this.getProcentageForTemplate();
  
  }


  exportToPDF() {
    let doc = new jsPDF();
    doc.fromHTML($('#report').get(0), 20, 20, {'width': 500});
    doc.save('Report.pdf')
  }

}
