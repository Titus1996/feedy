import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { UsersService } from 'app/Shared/Services/users.service';
import { LoginService } from 'app/Shared/Services/login.service';
import { Router } from '@angular/router';
import { User } from 'app/Shared/Model/user';

@Component({
  selector: 'app-team-inspect',
  templateUrl: './team-inspect.component.html',
  styleUrls: ['./team-inspect.component.css']
})
export class TeamInspectComponent implements OnInit {
  users: User[] = [];
  displayedColumns = ['fullName','team','email','feedback'];

  dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator)
  private paginator:MatPaginator;

  @ViewChild(MatSort)
  private sort: MatSort;

  constructor(private usersService: UsersService, private loginService: LoginService, private router: Router) {}

  getRole() {
    return this.loginService.getRole();
  }

  getFeedbacks(id:number){
      this.router.navigate(['/Feedbacks'],{ queryParams: {feedbacks : 'recived', id : id, sulInspecting : true} })
  }

  setUsers() {
    this.usersService.getUsersByTeam().subscribe(users =>{
        users.sort((u1,u2)=> u1.id - u2.id);
        this.users = users.filter(user=> user.name !== this.getUser());
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  getUser() {
    return this.loginService.getUser();
  }

  ngOnInit(): void {                
    this.setUsers();
  }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
}
