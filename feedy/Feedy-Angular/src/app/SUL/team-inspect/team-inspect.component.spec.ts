import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamInspectComponent } from './team-inspect.component';

describe('TeamInspectComponent', () => {
  let component: TeamInspectComponent;
  let fixture: ComponentFixture<TeamInspectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamInspectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamInspectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
