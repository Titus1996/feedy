import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';
import {Team} from '../Model/team';
import {User} from '../Model/user';
import {BaseURL} from '../BaseURL';
import {HeaderBuilder} from '../header-builder';
import {LoginService} from './login.service';

@Injectable()
export class UsersService {

  private getURL = BaseURL.getURL() + '/getAllUsers';
  private deleteURL = BaseURL.getURL() + '/deleteUser';
  private saveURL = BaseURL.getURL() + '/saveUser';
  private usersByTeamURL = BaseURL.getURL() + "/getTeamUsers"
  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {}

  getUsers(): Observable<User[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.getURL, this.options)
      .map(response => response.json() as User[]);
  }

  saveUser(user: User): Observable<any> {
    return this.http.post(this.saveURL, JSON.stringify(user), this.options);
  }

  deleteUser(id: number): Observable<any> {
    let deleteUrl = this.deleteURL + '/' + id;
    return this.http.delete(deleteUrl, this.options);
  }

  getUsersByTeam():Observable<User[]>{
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.usersByTeamURL,this.options)
      .map(res=>res.json() as User[]);
  }
}
