import {Injectable} from '@angular/core';
import {BaseURL} from '../BaseURL';
import {Http, RequestOptions} from '@angular/http';
import {LoginService} from './login.service';
import {HeaderBuilder} from '../header-builder';
import {Observable} from 'rxjs/Observable';
import {Template} from '../Model/template';
import {Question} from '../Model/question';

@Injectable()
export class QuestionService {

  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
  }


}
