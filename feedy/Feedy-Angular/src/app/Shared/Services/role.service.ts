import {Injectable} from '@angular/core';
import {HeaderBuilder} from '../header-builder';
import {LoginService} from './login.service';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {BaseURL} from '../BaseURL';

@Injectable()
export class RoleService {
  private roleURL = BaseURL.getURL() + '/roles';

  constructor(private loginService: LoginService, private http: Http) { }

  getRoles(): Observable<string[]> {
    let options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.roleURL, options).map(res => res.json() as string[])
      .catch(err => Observable.throw(err))
  }

}
