import {Injectable} from '@angular/core';
import {BaseURL} from '../BaseURL';
import {LoginService} from './login.service';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {HeaderBuilder} from '../header-builder';

@Injectable()
export class TeamService {
  private teamURL = BaseURL.getURL() + '/teams';

  constructor(private loginService: LoginService, private http: Http) {}

  getTeams(): Observable<string[]> {
    let options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.teamURL, options).map(res => res.json() as string[])
      .catch(err => Observable.throw(err))
  }

}
