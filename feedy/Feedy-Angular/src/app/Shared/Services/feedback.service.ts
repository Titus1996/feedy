import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {BaseURL} from '../BaseURL';
import {HeaderBuilder} from '../header-builder';
import {LoginService} from './login.service';
import {Feedback} from '../Model/feedback';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class FeedbackService {

  private recivedFeedbacksURL = BaseURL.getURL() + '/getReceivedFeedbacks';
  private sentFeedbacksURL = BaseURL.getURL() + '/getSentFeedbacks';
  private saveFeedbackURL = BaseURL.getURL() + '/saveFeedback';
  private markFeedback = BaseURL.getURL() + '/markFeedback';
  private getFeedbacksForUserURL = BaseURL.getURL() + '/getFeedbacksForUser';
  private recivedUnreadFeedbacksURL = BaseURL.getURL()  + '/getReceivedUnreadFeedbacks';
  private genericFeedbacksURL = BaseURL.getURL() + '/getGenericFeedbacks';
  private deleteFeedbackURL = BaseURL.getURL() + '/deleteFeedback';
  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {

  }

  public getFeedbacksRecived(): Observable<Feedback[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.recivedFeedbacksURL, this.options).map(res => res.json() as Feedback[]);
  }

  public deleteFeedback(id: number){
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let deleteURL = this.deleteFeedbackURL + '/' + id;
    return this.http.delete(deleteURL, this.options).map(res=> res);
  }

  public getFeedbacksSent(): Observable<Feedback[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let url = this.sentFeedbacksURL;
    return this.http.get(url, this.options).map(res => res.json() as Feedback[]);
  }

  public getUnreadFeedbacksRecived(): Observable<Feedback[]>{
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let url = this.recivedUnreadFeedbacksURL;
    return this.http.get(url, this.options).map(res => res.json() as Feedback[]);
  }

  public getGenericFeedbacks(): Observable<Feedback[]>{
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let url = this.genericFeedbacksURL;
    return this.http.get(url, this.options).map(res => res.json() as Feedback[]);
  }

  public saveFeedback(Feedback: Feedback) {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.post(this.saveFeedbackURL, JSON.stringify(Feedback), this.options);
  }

  public markFeedbackAsRead(feedback: Feedback){
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.post(this.markFeedback,JSON.stringify(feedback),this.options);
  }

  public getMessagesForUser(id:number){
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let url = this.getFeedbacksForUserURL + "/" + id;
    return this.http.get(url, this.options).map(res => res.json() as Feedback[]);
  }
}
