import {TestBed, inject} from '@angular/core/testing';
import {
  HttpModule,
  Http,
  Response,
  ResponseOptions,
  BaseRequestOptions,
  XHRBackend
} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {LoginService} from './login.service';

describe('LoginService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        MockBackend,
        BaseRequestOptions,
        {provide: XHRBackend, useClass: MockBackend},
        LoginService,
      ]
    });
  });


  it('should set the token,user and role', inject([LoginService, XHRBackend], (service, mockBackend) => {

    const mockResponse = {
      Role: 'mockRole',
      User: 'mockUser',
      Token: 'mockToken'
    };
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(new Response(new ResponseOptions({
        status: 200,
        body: JSON.stringify(mockResponse)
      })));
    });
    service.login('mockuser', 'mockpassword').subscribe((response) => {
      }
    );
    expect(service.isLoggedIn()).toEqual(true);
    expect(service.getRole()).toEqual('mockRole');
    expect(service.getToken()).toEqual('mockToken');
  }));

});
