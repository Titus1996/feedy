import {Injectable} from '@angular/core';
import {BaseURL} from '../BaseURL';
import {LoginService} from './login.service';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {HeaderBuilder} from '../header-builder';
import {Template} from '../Model/template';
import {Question} from '../Model/question';
import {ResponseChoice} from "../Model/ResponseChoice";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class TemplateService {
  private templateURL = BaseURL.getURL() + '/templates';
  private saveTemplateURL = BaseURL.getURL() + '/saveTemplate';
  private templateByNameURL = BaseURL.getURL() + '/getTemplateByName';
  private getURL = BaseURL.getURL() + '/getQuestionsByTemplate';
  private getResponsesURL = BaseURL.getURL() + '/responseChoiceByQuestion';
  private deleteTemplateURL = BaseURL.getURL() + '/deleteTemplate';
  private genericTempplatesURL = BaseURL.getURL() + '/genericTemplates';
  private nonGenericTemplateURL = BaseURL.getURL() + '/getNonGenericTemplates';
  private options: RequestOptions;

  constructor(private loginService: LoginService, private http: Http) {}

  getTemplates(): Observable<Template[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.templateURL, this.options).map(res => res.json() as Template[])
      .catch(err => Observable.throw(err))
  }

  getNonGenericTemplates():Observable<Template[]>{
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.nonGenericTemplateURL, this.options).map(res => res.json() as Template[])
      .catch(err => Observable.throw(err))
  }

  getGenericTemplates(): Observable<Template[]>{
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.genericTempplatesURL, this.options).map(res => res.json() as Template[])
    .catch(err => Observable.throw(err))
  }

  saveTemplate(template: Template): Observable<any> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    template.author = this.loginService.getUser();
    
    return this.http.post(this.saveTemplateURL, JSON.stringify(template), this.options).map(res => res.json())
      .catch(err => Observable.throw(err) || 'error');
  }

  getTemplateByName(name: string): Observable<Template> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let findTemplateUrl = this.templateByNameURL + '/' + name;
    return this.http.get(findTemplateUrl, this.options).map(res => res.json() as Template)
      .catch(err => Observable.throw(err))
  }

  getQuestions(templateId: number): Observable<Question[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let getQuestionsURL = this.getURL + '/' + templateId;
    return this.http.get(getQuestionsURL, this.options)
      .map(response => response.json() as Question[]);
  }

  getResponseChoices(questionId: number): Observable<ResponseChoice[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let getURL = this.getResponsesURL + '/' + questionId;
    return this.http.get(getURL, this.options)
      .map(response => response.json() as ResponseChoice[]);
  }

  deleteTemplate(templateId: number): Observable<any> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.delete(this.deleteTemplateURL + '/' + templateId, this.options);
  }
}

