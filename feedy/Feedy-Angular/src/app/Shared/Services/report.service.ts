 import {Injectable} from '@angular/core';
import {BaseURL} from '../BaseURL';
import {LoginService} from './login.service';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {HeaderBuilder} from '../header-builder';

@Injectable()
export class ReportService {
  private answers4TemplateURL = BaseURL.getURL() + '/getTopAnswersForTemplateQuestions';
  private procentage4FeedbackURL = BaseURL.getURL() + '/getProcentageForFeedback';
  private templatesByUsageURL = BaseURL.getURL() + '/getTemplatesByUsage';
  private options: RequestOptions;

  constructor(private loginService: LoginService, private http: Http) { }

  GetTopAnswersForTemplateQuestions(templateId: number): Observable<Map<string, Map<string, number>>[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let url = this.answers4TemplateURL + '/' + templateId;
    return this.http.get(url, this.options).map(res => res.json() as Observable<Map<string, Map<string, number>>[]>)
      .catch(err => Observable.throw(err));
  }

  GetProcentageForFeedback(templateId: number): Observable<number> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let url = this.procentage4FeedbackURL + '/' + templateId;
    return this.http.get(url, this.options).map(res => res.json() as number)
      .catch(err => Observable.throw(err));
  }

  GetTemplatesByUsage(): Observable<Map<string, number>> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.templatesByUsageURL, this.options).map(res => res.json() as Map<string, number>)
      .catch(err => Observable.throw(err));
  }
}
