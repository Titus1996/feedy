import { Injectable } from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {BaseURL} from '../BaseURL';
import { Email } from 'app/Shared/Model/email';
import {HeaderBuilder} from '../header-builder';
import { LoginService } from 'app/Shared/Services/login.service';

@Injectable()
export class EmailService {
  private sendEmailURL = BaseURL.getURL() + "/sendEmail";
  private options: RequestOptions;
  constructor(private http: Http, private loginService: LoginService) { }

  public sendEmail(email:Email){
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
      return this.http.post(this.sendEmailURL,JSON.stringify(email),this.options);
  }

}
