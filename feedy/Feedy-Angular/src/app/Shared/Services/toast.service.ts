import {Injectable} from '@angular/core';
import {LoginService} from './login.service';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {BaseURL} from '../BaseURL';
import {HeaderBuilder} from '../header-builder';
import {Message} from '../Model/message';

@Injectable()
export class ToastService {

  private getURL = BaseURL.getURL() + '/getMessages';
  private sendURL = BaseURL.getURL() + '/sendMessage';
  private dismissURL = BaseURL.getURL() + '/dismissMessage';
  private options: RequestOptions;

  constructor(private loginService: LoginService, private http: Http) { }

  public getMessages(receiver: String): Observable<Message[]> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    let url = this.getURL + '/' + receiver;
    return this.http.get(url, this.options).map(response => response.json() as Message[]);
  }

  public sendMessage(message: Message): Observable<any> {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.post(this.sendURL, JSON.stringify(message), this.options).catch(err => Observable.throw(err) || 'error');;
  }

  public dismissMessage(id: number): Observable<any>  {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.post(this.dismissURL + '/' + id,  JSON.stringify(id), this.options);
  }

}

