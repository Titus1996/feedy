import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../Services/login.service';
import {Question} from '../../Model/question';
import {TemplateService} from '../../Services/template.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ResponseChoice} from '../../Model/ResponseChoice';
import {FormGroup} from '@angular/forms';
import {Answer} from '../../Model/answer';
import {Feedback} from '../../Model/feedback';
import {FeedbackService} from '../../Services/feedback.service';
import {ToastService} from '../../Services/toast.service';
import {ToastrService} from 'ngx-toastr';
import {ToastMessagesComponent} from '../../toast-messages/toast-messages.component';
import {Message} from '../../Model/message';
import { EmailService } from 'app/Shared/Services/email.service';
import { Email } from 'app/Shared/Model/email';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  questions: Question[] = [];
  responseChoices: ResponseChoice[] = [];
  answers: any[] = [];
  public myForm: FormGroup;
  id: number;
  anonymous: boolean;
  To: string;
  step = 1;
  private sub: any;
  showTicks:false;
  autoTicks:false;
  errorMessage  = false;

  constructor(private loginService: LoginService, private templateService: TemplateService,
              private route: ActivatedRoute, private feedBackService: FeedbackService, private router: Router,
              private toastr: ToastrService, private toast: ToastService, private emailService: EmailService) {
    this.getQuestions();
  }

  getQuestions() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.id = params['tempalteID'];
      this.To = params['username'];
    });
    this.templateService.getQuestions((this.id)).subscribe(questions => {
      this.questions = questions;
      for (let i = 0; i < this.questions.length; i++) {
        if (this.questions[i].questionType === 'MULTIPLE_CHOICE') {
          let x = [];
          for (let j = 0; j < this.questions[i].responseChoices.length; j++) {
            x.push('');
          }
          this.answers[i] = x;
        }
      }
    });
  }


  ngOnInit() {
  }


  validateFeedback(feedback: Feedback){
    let answers = feedback.answers as any[];
    for(let i=0;i<answers.length;i++){
      if(answers[i].question.required) {
        if(answers[i].question.questionType === 'RATING' && answers[i].text === undefined){
          answers[i].text = answers[i].question.min;
          answers[i].score = answers[i].question.min;
        }
        if(answers[i].text === undefined || answers[i].length === 0){
            return false;
        }
      }
    }
    return true;
  }


  onSubmit() {
    let feedback = new Feedback();
    feedback.answers = [];
    for (let i = 0; i < this.questions.length; i++) {
      if (this.questions[i].questionType === 'MULTIPLE_CHOICE') {
        for (let j = 0; j < this.answers[i].length; j++) {
          let answer = new Answer();
          if (this.answers[i][j] === true) {
            answer.text = this.questions[i].responseChoices[j].responseChoiceText;
            answer.question = this.questions[i];
            feedback.answers.push(answer);
          }

        }
      }
      else {
        let answer = new Answer();
        if(this.questions[i].questionType === 'RATING'){
          answer.score = this.answers[i];
        }
        answer.text = this.answers[i];
        answer.question = this.questions[i];
        feedback.answers.push(answer);
      }
    }
    if(!this.validateFeedback(feedback)){
      this.errorMessage = true;
      return;
    }
      
    feedback.from = this.loginService.getUser();

    if(this.To !== undefined){
      feedback.to = this.To;
    } else{
        feedback.to = "";
        feedback.isGeneric = true;
    }
    if (this.anonymous === undefined) {
      feedback.anonymous = false;
    }
    else {
      feedback.anonymous = this.anonymous;
    }
    feedback.date = Date.now().toLocaleString();
    this.feedBackService.saveFeedback(feedback).subscribe(res => {
      let resCheck = res.text() === 'false';
      if(!resCheck){
        this.successToast();
        if(feedback.anonymous){
          feedback.from = 'Anonymous';
        }
        if(!feedback.isGeneric){
          let email =new Email(feedback.from, feedback.to, feedback.anonymous, this.id, feedback);
          this.emailService.sendEmail(email).subscribe();
        }
      }
      else{
        this.failureToast();
      }
      },
      err => {
        this.failureToast()
      });
    this.router.navigate(['/colleagues']);
  }

  successToast() {
    this.toastr.success('Feedback Sent!', 'Success!');
  }

  failureToast() {
    this.toastr.error('Error sending feedback!', 'Error!');
  }
}
