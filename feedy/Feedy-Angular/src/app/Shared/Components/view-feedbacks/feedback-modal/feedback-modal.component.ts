import {Component, OnInit, Inject, ViewEncapsulation} from '@angular/core';
import {Feedback} from '../../../Model/feedback';
import {Question} from '../../../Model/question';
import {Answer} from '../../../Model/answer';
import {ResponseChoice} from '../../../Model/ResponseChoice';
import * as jsPDF from 'jspdf'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FeedbackService } from 'app/Shared/Services/feedback.service';
declare var $: any;
@Component({
  selector: 'app-feedback-modal',
  templateUrl: './feedback-modal.component.html',
  styleUrls: ['./feedback-modal.component.css']
})
export class FeedbackModalComponent implements OnInit {
  feedback: Feedback;
  isReceived:boolean;
  public questions: Question[] = [];
  

  constructor(public dialogRef: MatDialogRef<FeedbackModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private feedbackService: FeedbackService) {
        this.feedback = data.feedback;
        this.isReceived = data.received;
  }

  ngOnInit() {
    this.getQuestions();
  }

  formatQuestions(questions: Question[]) {
    for (let i = 0; i < questions.length; i++) {
      questions[i].questionText = questions[i].questionText[0].toUpperCase() + questions[i].questionText.slice(1);
    }
  }

  markAsRead(){
    this.feedbackService.markFeedbackAsRead(this.feedback).subscribe();
  }

  private getQuestions() {
    let answers = this.feedback.answers;
    for (let i = 0; i < answers.length; i++) {
      if (this.questions.find(question => question.id === answers[i].question.id) === undefined) {
        this.questions.push(answers[i].question);
      }
    }
    this.formatQuestions(this.questions);
  }

  public getAnswersForQuestion(question: Question): Answer[] {
    let result: Answer[] = [];
    this.feedback.answers.forEach(answer => {
      if (answer.question.id === question.id) {
        result.push(answer);
      }
    });
 
    return result;
  }
  public verifyAnswer(response: ResponseChoice, question: Question): boolean {
    let answers = this.getAnswersForQuestion(question);
    let ok = false;
    answers.forEach(answer => {
      if (answer.text === response.responseChoiceText) { ok = true; }
    });
    return ok;
  }
  close(){
    this.dialogRef.close();
  }
  download() {
    let doc = new jsPDF();
    doc.fromHTML($('#feedback').get(0), 20, 20, {'width': 500});
    console.log(doc);
    doc.save('Feedback.pdf')
  }
}
