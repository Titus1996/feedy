import { Component, OnInit, ViewEncapsulation, ViewChildren, ViewChild, QueryList, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Feedback } from '../../Model/feedback';
import { FeedbackService } from '../../Services/feedback.service';
import { LoginService } from '../../Services/login.service';
import { DialogService } from 'ng2-bootstrap-modal';
import { FeedbackModalComponent } from './feedback-modal/feedback-modal.component';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatSort } from '@angular/material';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-view-feedbacks',
  templateUrl: './view-feedbacks.component.html',
  styleUrls: ['./view-feedbacks.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ViewFeedbacksComponent implements OnInit {
  feedbacks: Feedback[] = [];
  dataSource: MatTableDataSource<Feedback>;
  private READ_TYPE = 'All';
  private UNREAD_TYPE = 'Unread';
  feedbackTypes = [this.READ_TYPE, this.UNREAD_TYPE];
  feedbackType: string = this.UNREAD_TYPE;
  displayedColumns = ['to', 'from', 'date', 'view'];
  received: Boolean = false;
  method: string;
  private userId: number = 0;
  isSulInspecting : boolean;
  type:string;
  @ViewChild(MatPaginator)
  private paginator: MatPaginator;

  @ViewChild(MatSort)
  private sort: MatSort;

  constructor(private feedBackService: FeedbackService, private loginService: LoginService, private route: ActivatedRoute, private dialog: MatDialog) { }

  checkIfSelect(){
    return this.received && !this.isSulInspecting;
  }

  getFeedbacksForUser(){
    this.feedBackService.getMessagesForUser(this.userId)
      .subscribe(feedbacks => {
        this.feedbacks = feedbacks;
        this.dataSource = new MatTableDataSource<Feedback>(this.feedbacks);
        this.dataSource.paginator = this.paginator;
      },
      err => Observable.throw(err));
  }

  ngOnInit() { 
    this.route.queryParams.subscribe(params => {
      this.type = params['feedbacks'];
      this.userId = params['id'];
      this.isSulInspecting = params['sulInspecting'] == 'true';
      if(this.isSulInspecting){
        this.getFeedbacksForUser()

      }else{
        this.setFeedbacks()
      }
    });
  }

  getFeedbacksRecived() {
    if (this.feedbackType === this.READ_TYPE) {
      this.feedBackService.getFeedbacksRecived()
        .subscribe(feedbacks => {
          this.feedbacks = feedbacks;
          this.dataSource = new MatTableDataSource<Feedback>(this.feedbacks);
          this.dataSource.paginator = this.paginator;
        },
        err => Observable.throw(err));
    }
    else {
      this.feedBackService.getUnreadFeedbacksRecived()
        .subscribe(feedbacks => {
          this.feedbacks = feedbacks;
          this.dataSource = new MatTableDataSource<Feedback>(this.feedbacks);
          this.dataSource.paginator = this.paginator;
        });
    }
  }

  setFeedbacks() {
        if (this.type === 'received') {
          this.received = true;
          this.getFeedbacksRecived();
        }
        else {
          this.received = false;
          this.feedBackService.getFeedbacksSent()
            .subscribe(feedbacks => {
              this.feedbacks = feedbacks;
              this.dataSource = new MatTableDataSource(this.feedbacks);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            });
        }
    }
 

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ViewFeedback(feedback: Feedback) {
    this.dialog.open(FeedbackModalComponent, {
      data: {
        feedback: feedback,
        received: this.received
      },
      panelClass:"feedback-wrapper",
      width: "40vw",
      height: "65vh"

    });
  }

  getReceived() {
    return this.received;
  }
}

