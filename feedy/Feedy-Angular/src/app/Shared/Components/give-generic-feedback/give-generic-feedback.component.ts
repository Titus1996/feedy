import { Component, OnInit } from '@angular/core';
import { Template } from 'app/Shared/Model/template';
import { TemplateService } from 'app/Shared/Services/template.service';
import { LoginService } from 'app/Shared/Services/login.service';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-give-generic-feedback',
  templateUrl: './give-generic-feedback.component.html',
  styleUrls: ['./give-generic-feedback.component.css']
})
export class GiveGenericFeedbackComponent implements OnInit {

  availableTemplates:Template[];
  selectedTemplate:Number;

  constructor(private templateService:TemplateService, private loginService: LoginService,
    private router: Router) {
      this.templateService.getGenericTemplates().subscribe(res=>{
        this.availableTemplates = res;
      });
   }

  ngOnInit() {
  }

  goToTemplate(){
    console.log(this.selectedTemplate);
    if(this.selectedTemplate === undefined){
      return;
    }
 
    let username = this.loginService.getUser();
    let navExtras: NavigationExtras = {
      queryParams: {
        'tempalteID': this.selectedTemplate
      }
    }
    this.router.navigate(['feedback'], navExtras);
  }

}
