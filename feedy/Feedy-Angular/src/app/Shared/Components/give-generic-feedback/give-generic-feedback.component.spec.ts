import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveGenericFeedbackComponent } from './give-generic-feedback.component';

describe('GiveGenericFeedbackComponent', () => {
  let component: GiveGenericFeedbackComponent;
  let fixture: ComponentFixture<GiveGenericFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveGenericFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveGenericFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
