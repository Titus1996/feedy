import {Component, OnInit, Output, ViewChild} from '@angular/core';
import {User} from '../../Model/user';
import {Team} from '../../Model/team';
import {UsersService} from '../../Services/users.service';
import {LoginService} from '../../Services/login.service';
import {TemplateService} from '../../Services/template.service';
import {Template} from '../../Model/template';
import {isNull} from 'util';
import {NavigationExtras, Router} from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-colleagues',
  templateUrl: './colleagues.component.html',
  styleUrls: ['./colleagues.component.css'],
})

export class ColleaguesComponent implements OnInit {
  selectedTemplates: Map<string,number> = new Map();
  users: User[] = [];
  templates: Template[] = [];
  templateId: number;

  displayedColumns = ['fullName','team','email','feedback'];

  dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator)
  private paginator:MatPaginator;

  @ViewChild(MatSort)
  private sort: MatSort;

  constructor(private usersService: UsersService, private loginService: LoginService, private templateService: TemplateService, private router: Router) {}

  getRole() {
    return this.loginService.getRole();
  }

  setUsers() {
    this.usersService.getUsers().subscribe(users =>{
        users.sort((u1,u2)=> u1.id - u2.id);
        this.dataSource = new MatTableDataSource(users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
  getUser() {
    return this.loginService.getUser();
  }

  setTemplates() {
    this.templateService.getNonGenericTemplates().subscribe(templates => this.templates = templates);
  }

  ngOnInit(): void {               
    this.setTemplates();    
    this.setUsers();
  }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  setTemplateId(username: string) {
    if(this.selectedTemplates[username] === undefined || this.selectedTemplates[username].length===0){
      return;
    }
    let navExtras: NavigationExtras = {
      queryParams: {
        'tempalteID': this.selectedTemplates[username],
        'username': username
      }
    }
    this.router.navigate(['feedback'], navExtras);
  } 
}
