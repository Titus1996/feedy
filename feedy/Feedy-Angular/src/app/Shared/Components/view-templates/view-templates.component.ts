import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {TemplateService} from '../../Services/template.service';
import {Template} from '../../Model/template';
import {LoginService} from '../../Services/login.service';
import {ActivatedRoute} from '@angular/router';
import { EditTemplateComponent } from 'app/SUL/edit-template/edit-template.component';
import { MatDialog } from '@angular/material';
import { DeleteQuestionModalComponent } from 'app/Shared/Components/view-templates/delete-question-modal/delete-question-modal.component';
@Component({
  selector: 'app-view-templates',
  templateUrl: './view-templates.component.html',
  styleUrls: ['./view-templates.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class ViewTemplatesComponent implements OnInit {
  templates: Template[] = [];

  constructor(private templateService: TemplateService, private loginService: LoginService, private dialog: MatDialog
, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.setTemplates();
  }

  setTemplates() {
    this.templateService.getTemplates().subscribe(templates => this.templates = templates);
  }

  ViewTemplate(template: Template) {
    let disposable = this.dialog.open(EditTemplateComponent, {
      data:{template: template},
      height:"70vh",
      width:"80vw",
      panelClass:"dialog-wrapper"
    });
    disposable.afterClosed().subscribe(res =>{
      this.setTemplates();
    });
  }

  deleteTemplate(templateId: number){
    let res;
    let disposable = this.dialog.open(DeleteQuestionModalComponent,{
      width:"15vw",
    });
    disposable.afterClosed().subscribe(res =>{
        if(res === true ){
          this.templateService.deleteTemplate(templateId).subscribe(x =>
            this.setTemplates()
          );
        }
    });
  }
}
