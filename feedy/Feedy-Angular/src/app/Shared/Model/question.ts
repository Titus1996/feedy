import {ResponseChoice} from './ResponseChoice';

export class Question {
  id = 0;
  questionText: string;
  questionType: string;
  remark: string;
  responseChoices: ResponseChoice[];
  min:Number;
  max:Number;
  required:boolean;
}
