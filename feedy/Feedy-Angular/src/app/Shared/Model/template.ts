import {Question} from './question';

export class Template {
  id = 0;
  name: string;
  author: string;
  questions: Question[];
  isGeneric:boolean;
}
