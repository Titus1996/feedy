export class Message {
  id: number;
  receiver: string;
  sender: string;
  content: string;
  dismissed: boolean = false;
  constructor(receiver, sender, content) {
    this.id = 0;
    this.receiver = receiver;
    this.sender = sender;
    this.content = content;
  }
}
