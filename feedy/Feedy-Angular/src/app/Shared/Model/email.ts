import { Feedback } from "app/Shared/Model/feedback";

export class Email{
    public constructor( PFrom:string, PTo:string, PAnonymous:boolean, PTemplateUsed:number, feedback:Feedback ){
        this.From = PFrom;
        this.To = PTo;
        this.Anonymous=PAnonymous;
        this.TemplateUsed = PTemplateUsed;
        this.feedback = feedback;
    }
    From:string;
    To:string;
    Anonymous:boolean;
    TemplateUsed:number;
    feedback:Feedback;
}