import {Question} from './question';

export class Answer {
  id = 0;
  score:Number;
  text: string;
  question: Question;
}
