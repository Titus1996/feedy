import {Answer} from './answer';

export class Feedback {
  id = 0;
  from: string;
  to: string;
  anonymous: boolean;
  date: string;
  answers: Answer[];
  isGeneric:boolean;
}
