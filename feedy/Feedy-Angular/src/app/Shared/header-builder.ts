import {Headers, RequestOptions} from '@angular/http';

export class HeaderBuilder {
  public static getHeaders(token: string): Headers {
    let headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  public static getHeadersWithoutToken(): Headers {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  public static getOptions(token: string): RequestOptions {
    let options = new RequestOptions({headers: this.getHeaders(token)});
    return options;
  }
}
