import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasicuserComponent} from './basicuser.component';

describe('BasicuserComponent', () => {
  let component: BasicuserComponent;
  let fixture: ComponentFixture<BasicuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasicuserComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
