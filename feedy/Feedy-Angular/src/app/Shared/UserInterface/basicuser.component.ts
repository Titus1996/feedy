import {LoginService} from '../../Shared/Services/login.service';
import {Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import { MatSidenav } from '@angular/material';
@Component({
  selector: 'app-basicuser',
  templateUrl: './basicuser.component.html',
  styleUrls: ['./basicuser.component.css'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces:false
})
export class BasicuserComponent implements OnInit {
  userName: string;
  activeState: boolean[] = [];
  private ROLE_SUL = 'ROLE_SUL';
  constructor(private loginService: LoginService) {
  }

  ngOnInit() {
    this.setName();
    this.activeState[2] = true;
  }

  checkRole(){
    return this.loginService.getRole() === this.ROLE_SUL;
  }

  resetStates() {
    for (let i = 0; i < 3; ++i) {
      this.activeState[i] = false; }
  }

  

  setName() {
    this.userName = this.loginService.getFullName();
    if (this.userName === '') {
      alert('Unauthorized!');
      this.loginService.logout();
    }
  }

  logout() {
    this.loginService.logout();
  }
}
