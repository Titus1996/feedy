import {LoginService} from '../Services/login.service';
import {Component, OnInit} from '@angular/core';
import {ToastService} from '../Services/toast.service';
import {Message} from '../Model/message';
import {ToastrService} from 'ngx-toastr';
import {Observable} from 'rxjs/Observable';
import {Subscriber} from 'rxjs/Subscriber';

@Component({
  selector: './app-toast-messages',
  templateUrl: './toast-messages.component.html',
  styleUrls: ['./toast-messages.component.css'],
})

export class ToastMessagesComponent implements OnInit {
  messages: Message[] = [];

  /*notifications = new Observable<void>((observer: Subscriber<void>) => {
    setInterval(() => observer.next(this.showNotification()), 1000);
  });*/

  constructor(private loginService: LoginService, private toast: ToastService, private toastr: ToastrService) {
  }

  ngOnInit() {
    this.updateMessages();
  }

  public showNotification() {
    if(this.loginService.isLoggedIn()){
      this.updateMessages();
      for (let index = 0; index < this.messages.length; index ++) {
        if (this.messages[index].dismissed === false) {
          this.dismiss(this.messages[index].id);
          this.toastr.info(this.messages[index].content,
            'from ' + this.messages[index].sender,
            {
              closeButton: true,
              timeOut: 30000,
            })
        }
      }
    }
  }

  public updateMessages() {
    this.toast.getMessages(this.loginService.getUser()).subscribe(messages => this.messages = messages);
  }

  dismiss(id) {
    this.toast.dismissMessage(id).subscribe();
  }
}
