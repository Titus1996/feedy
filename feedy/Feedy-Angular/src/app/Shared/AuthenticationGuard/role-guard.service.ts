import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoginService } from 'app/Shared/Services/login.service';

@Injectable()
export class RoleGuardService {

  private SUL_ROLE = 'ROLE_SUL';

  constructor(private router: Router, private loginService: LoginService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.loginService.getRole() === this.SUL_ROLE) {
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }

}
