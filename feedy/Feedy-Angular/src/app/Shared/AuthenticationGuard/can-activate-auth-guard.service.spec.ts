import {TestBed, inject} from '@angular/core/testing';
import { CanActivateAuthGuardService } from 'app/Shared/AuthenticationGuard/can-activate-auth-guard.service';


describe('CanActivateAuthGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanActivateAuthGuardService]
    });
  });
});
