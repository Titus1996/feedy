import { environment } from '../../environments/environment';
export class BaseURL {
  private static URL = environment.baseURL;

  public static getURL(): string {
    return this.URL;
  }
}
