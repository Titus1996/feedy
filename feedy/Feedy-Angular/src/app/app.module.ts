
import {LoginService} from './Shared/Services/login.service';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {HomeComponent} from './Home/home/home.component';
import {AppRoutingModule} from './app.routing';
import {HttpModule} from '@angular/http';
import {BootstrapModalModule, DialogService} from 'ng2-bootstrap-modal';
import {UsersService} from './Shared/Services/users.service';
import {ColleaguesComponent} from './Shared/Components/colleagues/colleagues.component';
import {ExpandableListModule} from 'angular2-expandable-list';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderBuilder} from './Shared/header-builder';
import {RoleService} from './Shared/Services/role.service';
import {TeamService} from './Shared/Services/team.service';
import {ReportService} from './Shared/Services/report.service';
import {ToastService} from './Shared/Services/toast.service';
import { TemplateGeneratorComponent } from './SUL/template-generator/template-generator.component';
import { ReportGeneratorComponent } from './SUL/report-generator/report-generator.component'
import {TemplateService} from './Shared/Services/template.service';
import {FeedbackComponent} from './Shared/Components/feedback/feedback.component';
import {QuestionService} from './Shared/Services/question.service';
import { ViewFeedbacksComponent } from './Shared/Components/view-feedbacks/view-feedbacks.component';
import {FeedbackService} from './Shared/Services/feedback.service';
import { FeedbackModalComponent } from './Shared/Components/view-feedbacks/feedback-modal/feedback-modal.component';
import {ToastrModule} from 'ngx-toastr';
import { ViewTemplatesComponent } from './Shared/Components/view-templates/view-templates.component';
import 'hammerjs';
import {MatSidenavModule, MatSlideToggleModule} from '@angular/material'; 
import {ChartsModule} from 'ng2-charts';
import { EditTemplateComponent } from './SUL/edit-template/edit-template.component';
import { ReversePipe } from './reverse.pipe';
import { ToastMessagesComponent } from './Shared/toast-messages/toast-messages.component';
import { EmailService } from 'app/Shared/Services/email.service';
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list'
import { LoginComponent } from 'app/Home/home/login/login.component';
import {MatDialogModule} from '@angular/material/dialog';
import { MapToIterablePipe } from 'app/SUL/report-generator/Map_Pipe/map-to-iterable.pipe';
import { CanActivateAuthGuardService } from 'app/Shared/AuthenticationGuard/can-activate-auth-guard.service';
import { RoleGuardService } from 'app/Shared/AuthenticationGuard/role-guard.service';
import {BasicuserComponent} from './Shared/UserInterface/basicuser.component';
import { DeleteQuestionModalComponent } from 'app/Shared/Components/view-templates/delete-question-modal/delete-question-modal.component';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatSortModule} from '@angular/material/sort';
import { TeamInspectComponent } from './SUL/team-inspect/team-inspect.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {MatSliderModule} from '@angular/material/slider';
import {MatCardModule} from '@angular/material/card';
import { GiveGenericFeedbackComponent } from './Shared/Components/give-generic-feedback/give-generic-feedback.component';
import { ViewGenericFeedbacksComponent } from './SUL/view-generic-feedbacks/view-generic-feedbacks.component';
@NgModule({
  declarations: [
    LoginComponent,
    AppComponent,
    HomeComponent,
    BasicuserComponent,
    ColleaguesComponent,
    TemplateGeneratorComponent,
    FeedbackComponent,
    ViewFeedbacksComponent,
    FeedbackModalComponent,
    ViewTemplatesComponent,
    ReportGeneratorComponent,
    EditTemplateComponent,
    ReversePipe,
    ToastMessagesComponent,
    MapToIterablePipe,
    DeleteQuestionModalComponent,
    TeamInspectComponent,
    GiveGenericFeedbackComponent,
    ViewGenericFeedbacksComponent
  ],
  imports: [
    MatCardModule,
    MatSortModule,
    MatRadioModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatTableModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatListModule,
    MatTabsModule,
    MatSidenavModule,
    MatIconModule,
    ChartsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BootstrapModalModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatSlideToggleModule,
    ExpandableListModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
  ],
  exports:[ MatSlideToggleModule, MatCardModule, MatSliderModule, MatSortModule, MatRadioModule,
     MatCheckboxModule, MatListModule, MatTabsModule, MatIconModule, MatSidenavModule, MatDialogModule, MatInputModule, MatTableModule ],

  entryComponents: [
      LoginComponent, FeedbackModalComponent, EditTemplateComponent, DeleteQuestionModalComponent
  ],

    providers: [ LoginService, UsersService, HeaderBuilder, RoleService, TeamService, TemplateService, QuestionService,
    FeedbackService, ReportService, ToastService, EmailService, DialogService, CanActivateAuthGuardService, RoleGuardService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],

  bootstrap: [AppComponent],

})
export class AppModule {}
