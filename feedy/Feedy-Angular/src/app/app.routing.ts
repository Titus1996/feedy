 import { NgModule } from '@angular/core';
 import { Routes, RouterModule } from '@angular/router';
 import {TemplateGeneratorComponent} from './SUL/template-generator/template-generator.component';
 import { ReportGeneratorComponent} from './SUL/report-generator/report-generator.component';
 import {ColleaguesComponent} from './Shared/Components/colleagues/colleagues.component';
 import {FeedbackComponent} from './Shared/Components/feedback/feedback.component';
 import {ViewFeedbacksComponent} from './Shared/Components/view-feedbacks/view-feedbacks.component';
import { ViewTemplatesComponent} from './Shared/Components/view-templates/view-templates.component';
import { CanActivateAuthGuardService } from 'app/Shared/AuthenticationGuard/can-activate-auth-guard.service';
import { RoleGuardService } from 'app/Shared/AuthenticationGuard/role-guard.service';
import { TeamInspectComponent } from 'app/SUL/team-inspect/team-inspect.component';
import { GiveGenericFeedbackComponent } from 'app/Shared/Components/give-generic-feedback/give-generic-feedback.component';
import { ViewGenericFeedbacksComponent } from 'app/SUL/view-generic-feedbacks/view-generic-feedbacks.component';

 const routes: Routes = [
   {  path : 'generate' , component: TemplateGeneratorComponent, canActivate: [ CanActivateAuthGuardService ] },
   {  path: 'colleagues', component: ColleaguesComponent, canActivate: [ CanActivateAuthGuardService ] },
   {  path: 'feedback' , component: FeedbackComponent, canActivate: [ CanActivateAuthGuardService ] },
   {  path: 'Feedbacks' , component: ViewFeedbacksComponent, canActivate: [ CanActivateAuthGuardService ] },
   {  path: 'templates' , component: ViewTemplatesComponent, canActivate: [ CanActivateAuthGuardService, RoleGuardService ] },
   {  path: 'reports' , component: ReportGeneratorComponent, canActivate: [ CanActivateAuthGuardService, RoleGuardService ] },
   {  path: 'teamfeedback', component:TeamInspectComponent, canActivate: [ CanActivateAuthGuardService, RoleGuardService ] },
   {  path: 'genericFeedback', component:GiveGenericFeedbackComponent, canActivate:[CanActivateAuthGuardService] },
   {  path: 'genericFeedbacks', component:ViewGenericFeedbacksComponent, canActivate: [ CanActivateAuthGuardService, RoleGuardService ] }
 ];

 @NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule],
 })
 export class AppRoutingModule { }
